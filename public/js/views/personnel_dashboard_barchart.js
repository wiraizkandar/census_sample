$(function () {
    'use strict';
    var canvas = document.getElementById("pembanci_negeri");
    var canvas2 = document.getElementById("penyelia_negeri");
    var ctx1 = document.getElementById("pembanci_negeri").getContext('2d');
    var ctx2 = document.getElementById("penyelia_negeri").getContext('2d');

    var myChart1 = new Chart(ctx1, {
        type: 'bar',
        data: {
            labels: ["MALAYSIA", "Johor", "Kedah", "Kelantan", "Melaka", "Negeri Sembilan", "Pahang", "Pulau Pinang"
                        , "Perak", "Perlis", "Selangor", "Terengganu", "Sabah", "Sarawak", "W.P Kuala Lumpur", "W.P Labuan", "W.P Putrajaya"],
            datasets:
                    [{
                            label: 'Pembanci Disi',
                            data: [41, 22, 57, 19, 22, 34, 41, 22, 57, 19, 22, 34, 41, 22, 57, 19, 22],
                            backgroundColor: 'yellow'
                        },
                        {
                            label: 'Kosong',
                            data: [41, 22, 57, 19, 22, 34, 41, 22, 57, 19, 22, 34, 41, 22, 57, 19, 22],
                            backgroundColor: 'green'
                        },
                    ]
        },
        options: {
            legend: {
                display: true,
                position: 'bottom',
            },
            title: {
                display: true,
                text: 'Senarai Penghantaran/Penerimaan Dokumen Mengikut Negeri'
            },
            scales: {
                xAxes: [{
                        ticks: {
                            autoSkip: false,
                            fontSize: 9,
                        },
                        stacked: true,
                    }],
                yAxes: [{
                        ticks: {
                            beginAtZero: true,
                            steps: 10,
                            stepValue: 5,
                            max: 100
                        },
                        stacked: true
                    }]
            },
        }
    });


    var myChart2 = new Chart(ctx2, {
        type: 'bar',
        data: {
            labels: ["MALAYSIA", "Johor", "Kedah", "Kelantan", "Melaka", "Negeri Sembilan", "Pahang", "Pulau Pinang"
                        , "Perak", "Perlis", "Selangor", "Terengganu", "Sabah", "Sarawak", "W.P Kuala Lumpur", "W.P Labuan", "W.P Putrajaya"],
            datasets:
                    [{
                            label: 'Penyelia Disi',
                            data: [41, 22, 57, 19, 22, 34, 41, 22, 57, 19, 22, 34, 41, 22, 57, 19, 22],
                            backgroundColor: 'orange'
                        },
                        {
                            label: 'Kosong',
                            data: [41, 22, 57, 19, 22, 34, 41, 22, 57, 19, 22, 34, 41, 22, 57, 19, 22],
                            backgroundColor: 'blue'
                        },
                    ]
        },
        options: {
            legend: {
                display: true,
                position: 'bottom',
            },
            title: {
                display: true,
                text: 'Senarai Penghantaran/Penerimaan Dokumen Mengikut Negeri'
            },
            scales: {
                xAxes: [{
                        ticks: {
                            autoSkip: false,
                            fontSize: 9,
                        },
                        stacked: true,
                    }],
                yAxes: [{
                        ticks: {
                            beginAtZero: true,
                            steps: 10,
                            stepValue: 5,
                            max: 100
                        },
                        stacked: true
                    }]
            },
        }
    });

});
