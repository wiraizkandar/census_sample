<?php

namespace App\Http\Controllers\Inventory;

class InventoryAcceptancePostCensusController extends \App\Http\Controllers\Controller
{

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        
    }



    /**
     * Create the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('inventory.inventory_delivery_post_census_create');
    }



}
