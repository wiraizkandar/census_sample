@extends('master')
@section('content')

<div class="container-fluid">
    <div class="animated fadeIn">
        <div class="row">
            <!-- /.col-->
            <div class="col-lg-12">

                <div class="card">
                    <div class="card-header">
                        <i class="fa fa-align-justify"></i> Rekod Penerimaan Aset</div>
                    <div class="card-body">
                        <div class="form-group row">
                            <label class="col-md-2 col-form-label" for="select3">Nama Penerima</label>
                            <div class="col-md-4">
                                <select class="form-control form-control-sm" id="select3" name="select3">
                                    <option value="0"></option>
                                    <option value="1">Option #1</option>
                                    <option value="2">Option #2</option>
                                    <option value="3">Option #3</option>
                                </select>
                            </div>
                            <label class="col-md-2 col-form-label" for="select3">Jawatan</label>
                            <div class="col-md-4">
                                <select class="form-control form-control-sm" id="select3" name="select3">
                                    <option value="0"></option>
                                    <option value="1">Option #1</option>
                                    <option value="2">Option #2</option>
                                    <option value="3">Option #3</option>
                                </select>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-md-2 col-form-label" for="select3">Negeri</label>
                            <div class="col-md-4">
                                <select class="form-control form-control-sm" id="select3" name="select3">
                                    <option value="0"></option>
                                    <option value="1">Option #1</option>
                                    <option value="2">Option #2</option>
                                    <option value="3">Option #3</option>
                                </select>
                            </div>
                            <label class="col-md-2 col-form-label" for="select3">Daerah Pentadbiran/Jawatan</label>
                            <div class="col-md-4">
                                <select class="form-control form-control-sm" id="select3" name="select3">
                                    <option value="0">Sila pilih</option>
                                    <option value="1">Option #1</option>
                                    <option value="2">Option #2</option>
                                    <option value="3">Option #3</option>
                                </select>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-md-2 col-form-label" for="select3">Nombor Daerah Dibanci</label>
                            <div class="col-md-4">
                                <input class="form-control form-control-sm" id="input1-group2" type="text" name="input1-group2" placeholder="Nombor Daerah Dibanci">
                            </div>
                            <label class="col-md-2 col-form-label" for="select3">Nombor Lingkungan Banci</label>
                            <div class="col-md-4">
                                <input class="form-control form-control-sm" id="input1-group2" type="text" name="input1-group2" placeholder="Nombor Lingkungan Banci">
                            </div>
                            <br><br>
                            <label class="col-md-2 col-form-label" for="select3">Nombor Blok Perhitungan</label>
                            <div class="col-md-4">
                                <input class="form-control form-control-sm" id="input1-group2" type="text" name="input1-group2" placeholder="Nombor Blok Perhitungan">
                            </div>
                        </div>
                        <br/>
                        <br/>
                        <br/>

                        <div class="form-group row">
                            <label class="col-md-2 col-form-label" for="select3">Nama Penghantar</label>
                            <div class="col-md-4">
                                <select class="form-control form-control-sm" id="select3" name="select3">
                                    <option value="0"></option>
                                    <option value="1">Option #1</option>
                                    <option value="2">Option #2</option>
                                    <option value="3">Option #3</option>
                                </select>
                            </div>
                            <label class="col-md-2 col-form-label" for="select3">Jawatan</label>
                            <div class="col-md-4">
                                <select class="form-control form-control-sm" id="select3" name="select3">
                                    <option value="0"></option>
                                    <option value="1">Option #1</option>
                                    <option value="2">Option #2</option>
                                    <option value="3">Option #3</option>
                                </select>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-md-2 col-form-label" for="select3">Negeri</label>
                            <div class="col-md-4">
                                <select class="form-control form-control-sm" id="select3" name="select3">
                                    <option value="0"></option>
                                    <option value="1">Option #1</option>
                                    <option value="2">Option #2</option>
                                    <option value="3">Option #3</option>
                                </select>
                            </div>
                            <label class="col-md-2 col-form-label" for="select3">Daerah Pentadbiran/Jawatan</label>
                            <div class="col-md-4">
                                <select class="form-control form-control-sm" id="select3" name="select3">
                                    <option value="0">Sila pilih</option>
                                    <option value="1">Option #1</option>
                                    <option value="2">Option #2</option>
                                    <option value="3">Option #3</option>
                                </select>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-md-2 col-form-label" for="select3">Nombor Daerah Dibanci</label>
                            <div class="col-md-4">
                                <input class="form-control form-control-sm" id="input1-group2" type="text" name="input1-group2" placeholder="Nombor Daerah Dibanci">
                            </div>
                            <label class="col-md-2 col-form-label" for="select3">Nombor Lingkungan Banci</label>
                            <div class="col-md-4">
                                <input class="form-control form-control-sm" id="input1-group2" type="text" name="input1-group2" placeholder="Nombor Lingkungan Banci">
                            </div>
                            <br><br>
                            <label class="col-md-2 col-form-label" for="select3">Nombor Blok Perhitungan</label>
                            <div class="col-md-4">
                                <input class="form-control form-control-sm" id="input1-group2" type="text" name="input1-group2" placeholder="Nombor Blok Perhitungan">
                            </div>
                        </div>


                        <table class="table table-responsive-sm table-striped">
                            <thead>
                                <tr>
                                    <th>No</th>
                                    <th>Tarikh Hantar</th>
                                    <th>Kod Aset</th>
                                    <th>Jenis Aset</th>
                                    <th>Kuota</th>
                                    <th>Kuantiti Hantar</th>
                                    <th>Status</th>
                                    <th>Tindakan</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                </tr>
                                <tr>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                </tr>
                                <tr>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                </tr>
                                <tr>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                </tr>
                                <tr>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                </tr>
                                <tr>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                </tr>
                                <tr>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                </tr>
                            </tbody>
                        </table>
                        <left>
                            <button class="btn btn-sm btn-success" type="button">Tambah</button>
                        </left>
                        <center>
                            <button class="btn btn-primary" type="submit">Simpan</button>
                            <button class="btn btn-primary" type="submit">Hantar</button>
                            <button class="btn btn-dark" type="submit">Laporan</button>
                        </center>
                      
                    </div>
                </div>
            </div>
            <!-- /.col-->
        </div>
        @endsection
        <!-- /.conainer-fluid -->

