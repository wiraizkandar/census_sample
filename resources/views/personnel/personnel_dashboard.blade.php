@extends('master')
@section('content')

<div class="container-fluid">

    <div class="animated fadeIn">
        <div class="row">
            <div class="col-sm-7">
                <canvas id="pembanci_negeri"  height="200"></canvas>
            </div>
            <div class="col-sm-5">
                <canvas id="pie_pembanci"  height="200"></canvas>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-7">
                <canvas id="penyelia_negeri"  height="200"></canvas>
            </div>
            <div class="col-sm-5">
                <canvas id="pie_penyelia"  height="200"></canvas>
            </div>
        </div>
    </div>

</div>
@endsection
<!-- /.conainer-fluid -->

@section('myscript')
<script src="{{ asset('js/views/personnel_dashboard_barchart.js') }}"></script>
<script src="{{ asset('js/views/personnel_dashboard_piechart.js') }}"></script>
@endsection