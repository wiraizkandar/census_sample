1. composer install
2. copy .env.example and rename as .env
3. .env file 
4. Create database
5. set db credential in .env
6. set APP_URL in .env
7. php artisan migrate --seed
8. php artisan serve