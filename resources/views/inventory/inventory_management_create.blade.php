@extends('master')
@section('content')

<div class="container-fluid" style="background-color: white;">
    <div style="font-size: 24px;font-weight: bold;">Maklumat Inventori</div><br>
    
    <div class="form-group row">
        <label class="col-md-2 col-form-label" for="select3">Nama Inventori</label>
            <div class="col-md-4">
                <select class="form-control form-control-sm" id="select3" name="select3">
                    <option value="0">Sila pilih</option>
                    <option value="1">Option #1</option>
                    <option value="2">Option #2</option>
                    <option value="3">Option #3</option>
                </select>
            </div>
        <label class="col-md-2 col-form-label" for="select3">Muatnaik Inventori</label>
            <div class="col-md-4">
                <input type="file" class="form-control-file" id="exampleInputFile" aria-describedby="fileHelp">
            </div>
    </div>
    <div class="form-group row">
        <label class="col-md-2 col-form-label" for="select3">No Inventori</label>
            <div class="col-md-4">
                <select class="form-control form-control-sm" id="select3" name="select3">
                    <option value="0">Sila pilih</option>
                    <option value="1">Option #1</option>
                    <option value="2">Option #2</option>
                    <option value="3">Option #3</option>
                </select>
            </div>
        <label class="col-md-2 col-form-label" for="select3">Alamat premis</label>
            <div class="col-md-4">
                <textarea class="form-control" id="alamatpremis" name="textarea-input" rows="5" placeholder=""></textarea>
            </div>
    </div>
    <div class="form-group row">
        <label class="col-md-2 col-form-label" for="select3">Pembekal</label>
            <div class="col-md-4">
                <select class="form-control form-control-sm" id="select3" name="select3">
                    <option value="0">Sila pilih</option>
                    <option value="1">Option #1</option>
                    <option value="2">Option #2</option>
                    <option value="3">Option #3</option>
                </select>
            </div>
    </div>
    <div class="form-group row">
        <label class="col-md-2 col-form-label" for="select3">Kuantiti</label>
            <div class="col-md-4">
                <input type="text" class="form-control" id="kuantiti">
            </div>
        <label class="col-md-2 col-form-label" for="select3">No LO</label>
            <div class="col-md-4">
                <input type="text" class="form-control" id="nolo">
            </div>
    </div>
    <div class="form-group row">
        <label class="col-md-2 col-form-label" for="select3">Jenis Inventori</label>
            <div class="col-md-4">
                <input type="text" class="form-control" id="jenisdokumen">
            </div>
    </div>
    <div class="form-group row">
        <label class="col-md-2 col-form-label" for="select3">Perlupusan</label>
            <div class="col-md-4">
                    <label class="form-check-label">
                        <input type="checkbox" class="form-check-input">
                        <span style="font-size: 11px; color: grey;"><i>Sila klik untuk melupuskan dokumen</i></span>
                    </label>
            </div>
    </div>
    <div class="form-group row">
        <label class="col-md-4 col-form-label" for="select3">Sila tinggalkan komen/maklumbalas untuk melupuskan dokumen</label>
            <div class="col-md-6">
                <textarea style="background-color: grey;" class="form-control" id="komenlupus" name="textarea-input" rows="3" placeholder=""></textarea>
            </div>
    </div>
    <div>
        <br>
            <center>
                <button class="btn btn-success" type="submit">Kemaskini</button>
                <button class="btn btn-primary" type="submit">Hantar</button>
            </center>
        <br>
    </div>
</div>
@endsection
<!-- /.conainer-fluid -->

