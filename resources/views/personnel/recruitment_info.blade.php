@extends('master')
@section('content')

<div class="container-fluid">
    <div class="animated fadeIn">
        <div class="row">
            <!-- /.col-->
            <div class="col-lg-12">

                <div class="card">
                    <div class="card-header">
                        <i class="fa fa-align-justify"></i> Maklumat Peribadi</div>
                    <div class="card-body">
                        <div class="row">
                            <div class="col-md-2"  >
                                <i class="fa fa-user-circle" style="font-size:140px" ></i>
                            </div>                    
                            <div class="col-md-10"  >
                                <div class="form-group row">
                                    <label class="col-md-2 col-form-label" for="select3">Nama</label>
                                    <div class="col-md-4">
                                        <input class="form-control form-control-sm" id="input1-group2" type="text" name="input1-group2" placeholder="">
                                    </div>
                                    <label class="col-md-2 col-form-label" for="select3">No K/P</label>
                                    <div class="col-md-4">
                                        <input class="form-control form-control-sm" id="input1-group2" type="text" name="input1-group2" placeholder="">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-md-2 col-form-label" for="select3">Jantina</label>
                                    <div class="col-md-4">
                                        <input class="form-control form-control-sm" id="input1-group2" type="text" name="input1-group2" placeholder="">
                                    </div>
                                    <label class="col-md-2 col-form-label" for="select3">Alamat Surat Menyurat</label>
                                    <div class="col-md-4">
                                        <textarea class="form-control form-control-sm" ></textarea>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-md-2 col-form-label" for="select3">Bangsa</label>
                                    <div class="col-md-4">
                                        <input class="form-control form-control-sm" id="input1-group2" type="text" name="input1-group2" placeholder="">
                                    </div>
                                    <label class="col-md-2 col-form-label" for="select3">Taraf Perkahwinan</label>
                                    <div class="col-md-4">
                                        <input class="form-control form-control-sm" id="input1-group2" type="text" name="input1-group2" placeholder="">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-md-2 col-form-label" for="select3">Agama</label>
                                    <div class="col-md-4">
                                        <input class="form-control form-control-sm" id="input1-group2" type="text" name="input1-group2" placeholder="">
                                    </div>
                                    <label class="col-md-2 col-form-label" for="select3">Bandar</label>
                                    <div class="col-md-4">
                                        <input class="form-control form-control-sm" id="input1-group2" type="text" name="input1-group2" placeholder="">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-md-2 col-form-label" for="select3">No Tel Bimbit</label>
                                    <div class="col-md-4">
                                        <input class="form-control form-control-sm" id="input1-group2" type="text" name="input1-group2" placeholder="">
                                    </div>
                                    <label class="col-md-2 col-form-label" for="select3">Negeri</label>
                                    <div class="col-md-4">
                                        <input class="form-control form-control-sm" id="input1-group2" type="text" name="input1-group2" placeholder="">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-md-2 col-form-label" for="select3">Email</label>
                                    <div class="col-md-4">
                                        <input class="form-control form-control-sm" id="input1-group2" type="text" name="input1-group2" placeholder="">
                                    </div>
                                    <label class="col-md-2 col-form-label" for="select3">Daerah Pentadbiran</label>
                                    <div class="col-md-4">
                                        <input class="form-control form-control-sm" id="input1-group2" type="text" name="input1-group2" placeholder="">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-md-2 col-form-label" for="select3">Jenis Lesen</label>
                                    <div class="col-md-4">
                                        <input class="form-control form-control-sm" id="input1-group2" type="text" name="input1-group2" placeholder="">
                                    </div>
                                    <label class="col-md-2 col-form-label" for="select3">Daerah Banci/Lokasi</label>
                                    <div class="col-md-4">
                                        <input class="form-control form-control-sm" id="input1-group2" type="text" name="input1-group2" placeholder="">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-md-2 col-form-label" for="select3">Jawatan</label>
                                    <div class="col-md-4">
                                        <input class="form-control form-control-sm" id="input1-group2" type="text" name="input1-group2" placeholder="">
                                    </div>
                                </div>
                            </div>  
                        </div>
                    </div>
                </div>
                <div class="card">
                    <div class="card-header">
                        <i class="fa fa-align-justify"></i> Maklumat Pekerjaan</div>
                    <div class="card-body">
                        <div class="row">
                            <div class="col-md-12"  >
                                <div class="form-group row">
                                    <label class="col-md-2 col-form-label" for="select3">No Tel Pejabat</label>
                                    <div class="col-md-4">
                                        <input class="form-control form-control-sm" id="input1-group2" type="text" name="input1-group2" placeholder="">
                                    </div>
                                    <label class="col-md-2 col-form-label" for="select3">Nama JKK</label>
                                    <div class="col-md-4">
                                        <input class="form-control form-control-sm" id="input1-group2" type="text" name="input1-group2" placeholder="">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-md-2 col-form-label" for="select3">Tarikh Pewartaan</label>
                                    <div class="col-md-4">
                                        <div class="input-group">
                                            <input class="form-control form-control-sm" id="input1-group2" type="date" name="input1-group2" placeholder="Cari Nama Dokumen">
                                            <span class="input-group-prepend">
                                                <button class="btn btn-secondary" type="button">
                                                    <i class="fa fa-calendar"></i></button>
                                            </span>
                                        </div>
                                    </div>
                                    <label class="col-md-2 col-form-label" for="select3">No Bahagian</label>
                                    <div class="col-md-4">
                                        <textarea class="form-control form-control-sm" ></textarea>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-md-2 col-form-label" for="select3">Tempoh Pewartaan</label>
                                    <div class="col-md-4">
                                        <input class="form-control form-control-sm" id="input1-group2" type="text" name="input1-group2" placeholder="">
                                    </div>
                                    <label class="col-md-2 col-form-label" for="select3">Jenis Kenderaan</label>
                                    <div class="col-md-4">
                                        <input class="form-control form-control-sm" id="input1-group2" type="text" name="input1-group2" placeholder="">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-md-2 col-form-label" for="select3">Tarikh Lantikan</label>
                                    <div class="col-md-4">
                                        <div class="input-group">
                                            <input class="form-control form-control-sm" id="input1-group2" type="date" name="input1-group2" placeholder="Cari Nama Dokumen">
                                            <span class="input-group-prepend">
                                                <button class="btn btn-secondary" type="button">
                                                    <i class="fa fa-calendar"></i></button>
                                            </span>
                                        </div>
                                    </div>
                                    <label class="col-md-2 col-form-label" for="select3">Pemilikan Kenderaan</label>
                                    <div class="col-md-4">
                                        <input class="form-control form-control-sm" id="input1-group2" type="text" name="input1-group2" placeholder="">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-md-2 col-form-label" for="select3">Tarikh Penamatan</label>
                                    <div class="col-md-4">
                                        <div class="input-group">
                                            <input class="form-control form-control-sm" id="input1-group2" type="date" name="input1-group2" placeholder="Cari Nama Dokumen">
                                            <span class="input-group-prepend">
                                                <button class="btn btn-secondary" type="button">
                                                    <i class="fa fa-calendar"></i></button>
                                            </span>
                                        </div>
                                    </div>
                                    <label class="col-md-2 col-form-label" for="select3">Lesen Memandu</label>
                                    <div class="col-md-4">
                                        <input class="form-control form-control-sm" id="input1-group2" type="text" name="input1-group2" placeholder="">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-md-2 col-form-label" for="select3">Pejabat Rasmi Operasi Banci</label>
                                    <div class="col-md-4">
                                        <input class="form-control form-control-sm" id="input1-group2" type="text" name="input1-group2" placeholder="">
                                    </div>
                                    <label class="col-md-2 col-form-label" for="select3">Geran Kenderaan</label>
                                    <div class="col-md-4">
                                        <input class="form-control form-control-sm" id="input1-group2" type="text" name="input1-group2" placeholder="">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-md-2 col-form-label" for="select3">Alamat Pusat Prosesan Banci </label>
                                    <div class="col-md-4">
                                        <textarea class="form-control form-control-sm" ></textarea>
                                    </div>
                                    <label class="col-md-2 col-form-label" for="select3">No Polisi Insuran Kelompok</label>
                                    <div class="col-md-4">
                                        <input class="form-control form-control-sm" id="input1-group2" type="text" name="input1-group2" placeholder="">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-md-2 col-form-label" for="select3">Tarikah Temuduga </label>
                                    <div class="col-md-4">
                                        <input class="form-control form-control-sm" id="input1-group2" type="text" name="input1-group2" placeholder="">
                                    </div>
                                    <label class="col-md-2 col-form-label" for="select3">No Bank</label>
                                    <div class="col-md-4">
                                        <input class="form-control form-control-sm" id="input1-group2" type="text" name="input1-group2" placeholder="">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-md-2 col-form-label" for="select3">Status Temuduga(Lulus, Simpanan) </label>
                                    <div class="col-md-4">
                                        <input class="form-control form-control-sm" id="input1-group2" type="text" name="input1-group2" placeholder="">
                                    </div>
                                    <label class="col-md-2 col-form-label" for="select3">No Akaun Bank</label>
                                    <div class="col-md-4">
                                        <input class="form-control form-control-sm" id="input1-group2" type="text" name="input1-group2" placeholder="">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-md-2 col-form-label" for="select3">No KWSP </label>
                                    <div class="col-md-4">
                                        <input class="form-control form-control-sm" id="input1-group2" type="text" name="input1-group2" placeholder="">
                                    </div>
                                    <label class="col-md-2 col-form-label" for="select3">Kelayakan Honorarium</label>
                                    <div class="col-md-4">
                                        <input class="form-control form-control-sm" id="input1-group2" type="text" name="input1-group2" placeholder="">
                                    </div>
                                </div>
                            </div>  
                        </div>
                    </div>

                    <center>
                        <button class="btn btn-success" type="submit">Kemaskini</button>
                        <button class="btn btn-primary" type="submit">Hantar</button>
                    </center>
                    <br/>
                </div>
                
            </div>
        </div>
        <!-- /.col-->
    </div>
    @endsection
    <!-- /.conainer-fluid -->

