@extends('master')
@section('content')

<div class="container-fluid">
          <div class="animated fadeIn">
            <div class="row">
              <!-- /.col-->
              <div class="col-lg-12">

                <div class="card">
                  <div class="card-header">
                    <i class="fa fa-align-justify"></i> Maklumat Permintaan Aset Bagi Negeri</div>
                  <div class="card-body">
                    <div class="form-group row">
                        <label class="col-md-2 col-form-label" for="select3">Nama Pemohon</label>
                        <div class="col-md-4">
                          <select class="form-control form-control-sm" id="select3" name="select3">
                            <option value="0">Ali Bin Karim</option>
                            <option value="1">Option #1</option>
                            <option value="2">Option #2</option>
                            <option value="3">Option #3</option>
                          </select>
                        </div>
                        <label class="col-md-2 col-form-label" for="select3">Jawatan Pemohon</label>
                        <div class="col-md-4">
                          <select class="form-control form-control-sm" id="select3" name="select3">
                            <option value="0">Penyelia</option>
                            <option value="1">Option #1</option>
                            <option value="2">Option #2</option>
                            <option value="3">Option #3</option>
                          </select>
                        </div>
                      </div>
                      <div class="form-group row">
                        <label class="col-md-2 col-form-label" for="select3">Negeri Pemohon</label>
                        <div class="col-md-4">
                          <select class="form-control form-control-sm" id="select3" name="select3">
                            <option value="0">Johor</option>
                            <option value="1">Option #1</option>
                            <option value="2">Option #2</option>
                            <option value="3">Option #3</option>
                          </select>
                        </div>
                        <label class="col-md-2 col-form-label" for="select3">Negeri Dipohon</label>
                        <div class="col-md-4">
                          <select class="form-control form-control-sm" id="select3" name="select3">
                            <option value="0">Melaka</option>
                            <option value="1">Option #1</option>
                            <option value="2">Option #2</option>
                            <option value="3">Option #3</option>
                          </select>
                        </div>
                      </div>
                      <div class="form-group row">
                        <label class="col-md-2 col-form-label" for="select3">Nama Aset</label>
                        <div class="col-md-4">
                          <select class="form-control form-control-sm" id="select3" name="select3">
                            <option value="0">Soal Selidik</option>
                            <option value="1">Option #1</option>
                            <option value="2">Option #2</option>
                            <option value="3">Option #3</option>
                          </select>
                        </div>
                        <label class="col-md-2 col-form-label" for="select3">Kuantiti</label>
                        <div class="col-md-4">
                          <select class="form-control form-control-sm" id="select3" name="select3">
                            <option value="0">100</option>
                            <option value="1">Option #1</option>
                            <option value="2">Option #2</option>
                            <option value="3">Option #3</option>
                          </select>
                        </div>
                        <br><br>
                        <label class="col-md-2 col-form-label" for="select3">Tarikh Permintaan</label>
                        <div class="col-md-3">
                          <div class="input-group">
                            <input class="form-control form-control-sm" id="input1-group2" type="date" name="input1-group2" placeholder="Cari Nama Aset">
                            <span class="input-group-prepend">
                              <button class="btn btn-secondary" type="button">
                                <i class="fa fa-calendar"></i></button>
                            </span>
                          </div>
                        </div>
                        <div class="col-md-1"></div>
                        <label class="col-md-2 col-form-label" for="select3">Catatan</label>
                        <div class="col-md-4">
                          <textarea class="form-control" id="textarea-input" name="textarea-input" rows="9" placeholder="">Aset tidak mencukupi</textarea>
                          <br>
                        </div>
                        <div class="col-md-11"></div>
                          <button class="btn btn-sm btn-success" type="button">Tambah</button>
                      </div>
                    <table class="table table-responsive-sm table-striped">
                      <thead>
                        <tr>
                          <th>No</th>
                          <th>Tarikh Permintaan</th>
                          <th>Kod Aset</th>
                          <th>Nama Aset</th>
                          <th>Negeri Pemohon</th>
                          <th>Negeri Dipohon</th>
                          <th>Kuantiti</th>
                          <th>Status</th>
                          <th>Tindakan</th>
                        </tr>
                      </thead>
                      <tbody>
                        <tr>
                          <td>1</td>
                          <td>30/08/2018</td>
                          <td>2</td>
                          <td>Soal Selidik</td>
                          <td>Johor</td>
                          <td>Melaka</td>
                          <td>100</td>
                          <td><button class="btn btn-primary" type="submit">Lulus</button></td>
                          <td><button class="btn btn-sm btn-success" type="submit">
                      <i class="fa fa-dot-circle-o"></i></button>
                      <button class="btn btn-sm btn-danger" type="submit">
                      <i class="fa fa-dot-circle-o"></i></button></td>
                        </tr>
                        <tr>
                          <td></td>
                          <td></td>
                          <td></td>
                          <td></td>
                          <td></td>
                          <td></td>
                          <td></td>
                          <td></td>
                          <td></td>
                        </tr>
                        <tr>
                          <td></td>
                          <td></td>
                          <td></td>
                          <td></td>
                          <td></td>
                          <td></td>
                          <td></td>
                          <td></td>
                          <td></td>
                        </tr>
                        <tr>
                          <td></td>
                          <td></td>
                          <td></td>
                          <td></td>
                          <td></td>
                          <td></td>
                          <td></td>
                          <td></td>
                          <td></td>
                        </tr>
                        <tr>
                          <td></td>
                          <td></td>
                          <td></td>
                          <td></td>
                          <td></td>
                          <td></td>
                          <td></td>
                          <td></td>
                          <td></td>
                        </tr>
                        <tr>
                          <td></td>
                          <td></td>
                          <td></td>
                          <td></td>
                          <td></td>
                          <td></td>
                          <td></td>
                          <td></td>
                          <td></td>
                        </tr>
                        <tr>
                          <td></td>
                          <td></td>
                          <td></td>
                          <td></td>
                          <td></td>
                          <td></td>
                          <td></td>
                          <td></td>
                          <td></td>
                        </tr>
                      </tbody>
                    </table>
                    <center>
                      <!-- <button class="btn btn-primary" type="submit">Simpan</button> -->
                      <button class="btn btn-primary" type="submit">Hantar</button>
                    </center>
                  </div>
                </div>
              </div>
              <!-- /.col-->
            </div>
@endsection
<!-- /.conainer-fluid -->

