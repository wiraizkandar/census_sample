$(function () {
    'use strict';
    var ctx1 = document.getElementById("pie1");
    var ctx2 = document.getElementById("pie2");
    var ctx3 = document.getElementById("pie3");
    var ctx4 = document.getElementById("pie4");
    var ctx5 = document.getElementById("pie5");
    var ctx6 = document.getElementById("pie6");
    var ctx7 = document.getElementById("pie7");
    var ctx8 = document.getElementById("pie8");
    var ctx9 = document.getElementById("pie9");
    var ctx10 = document.getElementById("pie10");
    var ctx11 = document.getElementById("pie11");
    var ctx12 = document.getElementById("pie12");
    var ctx13 = document.getElementById("pie13");
    var ctx14 = document.getElementById("pie14");
    var ctx15 = document.getElementById("pie15");
    var ctx16 = document.getElementById("pie16");

    var myChart1 = new Chart(ctx1, {
        type: 'pie',
        data: {
            datasets: [{
                    data: [56, 33, 11],
                    backgroundColor: [
                        'blue',
                        'red',
                        'yellow',
                    ]
                }, ],

            labels: ["Jumlah Perkhidmatan", "Jumlah Pembekalan", "Jumlah Lain-Lain"]
        },
        options: {
            title: {
                display: true,
                text: 'Peratusan Jumlah Pembekal Mengikut Jenis Urusniaga'
            },
            responsive: true,
            legend: {
                display: true,
                position: 'bottom',
                labels: {
                    fontSize: 12,
                    usePointStyle: true
                },

            }
        }
    });
    
     var myChart2 = new Chart(ctx2, {
        type: 'pie',
        data: {
            datasets: [{
                    data: [40, 60],
                    backgroundColor: [
                        'yellow',
                        'blue',
                    ]
                }, ],

            labels: ["Selesai", "Belum Selesai"]
        },
        options: {
            title: {
                display: true,
                text: 'Soal Selidik'
            },
            responsive: true,
            legend: {
                display: true,
                position: 'bottom',
                labels: {
                    fontSize: 6,
                    usePointStyle: true
                },

            }
        }
    });
    
     var myChart3 = new Chart(ctx3, {
        type: 'pie',
        data: {
            datasets: [{
                    data: [44, 56],
                    backgroundColor: [
                        'yellow',
                        'blue',
                    ]
                }, ],

            labels: ["Selesai", "Belum Selesai"]
        },
        options: {
            title: {
                display: true,
                text: 'Soal Selidik Tempat Kediaman Beramai-ramai (TK dan IR)'
            },
            responsive: true,
            legend: {
                display: true,
                position: 'bottom',
                labels: {
                    fontSize: 6,
                    usePointStyle: true
                },

            }
        }
    });
    
     var myChart4 = new Chart(ctx4, {
        type: 'pie',
        data: {
            datasets: [{
                    data: [43, 57],
                    backgroundColor: [
                        'yellow',
                        'blue',
                    ]
                }, ],

            labels: ["Selesai", "Belum Selesai"]
        },
        options: {
            title: {
                display: true,
                text: 'Soal Selidik Tempat Kediaman Beramai-ramai (Perseorangan)'
            },
            responsive: true,
            legend: {
                display: true,
                position: 'bottom',
                labels: {
                    fontSize: 6,
                    usePointStyle: true
                },

            }
        }
    });
    
     var myChart5 = new Chart(ctx5, {
        type: 'pie',
        data: {
            datasets: [{
                    data: [70, 30],
                    backgroundColor: [
                        'yellow',
                        'blue',
                    ]
                }, ],

            labels: ["Selesai", "Belum Selesai"]
        },
        options: {
            title: {
                display: true,
                text: 'Kad Kod'
            },
            responsive: true,
            legend: {
                display: true,
                position: 'bottom',
                labels: {
                    fontSize: 6,
                    usePointStyle: true
                },

            }
        }
    });
    
    
     var myChart6 = new Chart(ctx6, {
        type: 'pie',
        data: {
            datasets: [{
                    data: [44, 56],
                    backgroundColor: [
                        'yellow',
                        'blue',
                    ]
                }, ],

            labels: ["Selesai", "Belum Selesai"]
        },
        options: {
            title: {
                display: true,
                text: 'Kad Penyenaraian Tempat Kediaman'
            },
            responsive: true,
            legend: {
                display: true,
                position: 'bottom',
                labels: {
                    fontSize: 6,
                    usePointStyle: true
                },

            }
        }
    });
    
     var myChart7 = new Chart(ctx7, {
        type: 'pie',
        data: {
            datasets: [{
                    data: [44, 56],
                    backgroundColor: [
                        'yellow',
                        'blue',
                    ]
                }, ],

            labels: ["Selesai", "Belum Selesai"]
        },
        options: {
            title: {
                display: true,
                text: 'Panduan Mengisi e-Census'
            },
            responsive: true,
            legend: {
                display: true,
                position: 'bottom',
                labels: {
                    fontSize: 6,
                    usePointStyle: true
                },

            }
        }
    });
    
     var myChart8 = new Chart(ctx8, {
        type: 'pie',
        data: {
            datasets: [{
                    data: [54, 46],
                    backgroundColor: [
                        'yellow',
                        'blue',
                    ]
                }, ],

            labels: ["Selesai", "Belum Selesai"]
        },
        options: {
            title: {
                display: true,
                text: 'Surat Lawatan Semula'
            },
            responsive: true,
            legend: {
                display: true,
                position: 'bottom',
                labels: {
                    fontSize: 6,
                    usePointStyle: true
                },

            }
        }
    });

    var myChart9 = new Chart(ctx9, {
        type: 'pie',
        data: {
            datasets: [{
                    data: [44, 56],
                    backgroundColor: [
                        'yellow',
                        'blue',
                    ]
                }, ],

            labels: ["Selesai", "Belum Selesai"]
        },
        options: {
            title: {
                display: true,
                text: 'Label Kotak Blok Perhitungan'
            },
            responsive: true,
            legend: {
                display: true,
                position: 'bottom',
                labels: {
                    fontSize: 6,
                    usePointStyle: true
                },

            }
        }
    });
    
     var myChart10 = new Chart(ctx10, {
        type: 'pie',
        data: {
            datasets: [{
                    data: [44, 56],
                    backgroundColor: [
                        'yellow',
                        'blue',
                    ]
                }, ],

            labels: ["Selesai", "Belum Selesai"]
        },
        options: {
            title: {
                display: true,
                text: 'Borang Kawalan e-Census'
            },
            responsive: true,
            legend: {
                display: true,
                position: 'bottom',
                labels: {
                    fontSize: 6,
                    usePointStyle: true
                },

            }
        }
    });
    
     var myChart11 = new Chart(ctx11, {
        type: 'pie',
        data: {
            datasets: [{
                    data: [44, 56],
                    backgroundColor: [
                        'yellow',
                        'blue',
                    ]
                }, ],

            labels: ["Selesai", "Belum Selesai"]
        },
        options: {
            title: {
                display: true,
                text: 'Kad Pengenalan Pembanci'
            },
            responsive: true,
            legend: {
                display: true,
                position: 'bottom',
                labels: {
                    fontSize: 6,
                    usePointStyle: true
                },

            }
        }
    });
    
     var myChart12 = new Chart(ctx12, {
        type: 'pie',
        data: {
            datasets: [{
                    data: [48, 52],
                    backgroundColor: [
                        'yellow',
                        'blue',
                    ]
                }, ],

            labels: ["Selesai", "Belum Selesai"]
        },
        options: {
            title: {
                display: true,
                text: 'Kad Pengenalan Penyelia'
            },
            responsive: true,
            legend: {
                display: true,
                position: 'bottom',
                labels: {
                    fontSize: 6,
                    usePointStyle: true
                },

            }
        }
    });
    
     var myChart13 = new Chart(ctx13, {
        type: 'pie',
        data: {
            datasets: [{
                    data: [63, 37],
                    backgroundColor: [
                        'yellow',
                        'blue',
                    ]
                }, ],

            labels: ["Selesai", "Belum Selesai"]
        },
        options: {
            title: {
                display: true,
                text: 'Kad Kod'
            },
            responsive: true,
            legend: {
                display: true,
                position: 'bottom',
                labels: {
                    fontSize: 6,
                    usePointStyle: true
                },

            }
        }
    });
    
    
     var myChart14 = new Chart(ctx14, {
        type: 'pie',
        data: {
            datasets: [{
                    data: [44, 56],
                    backgroundColor: [
                        'yellow',
                        'blue',
                    ]
                }, ],

            labels: ["Selesai", "Belum Selesai"]
        },
        options: {
            title: {
                display: true,
                text: 'Kad Penyenaraian Tempat Kediaman'
            },
            responsive: true,
            legend: {
                display: true,
                position: 'bottom',
                labels: {
                    fontSize: 6,
                    usePointStyle: true
                },

            }
        }
    });
    
     var myChart15 = new Chart(ctx15, {
        type: 'pie',
        data: {
            datasets: [{
                    data: [44, 56],
                    backgroundColor: [
                        'yellow',
                        'blue',
                    ]
                }, ],

            labels: ["Selesai", "Belum Selesai"]
        },
        options: {
            title: {
                display: true,
                text: 'Panduan Mengisi e-Census'
            },
            responsive: true,
            legend: {
                display: true,
                position: 'bottom',
                labels: {
                    fontSize: 6,
                    usePointStyle: true
                },

            }
        }
    });
    
     var myChart16 = new Chart(ctx16, {
        type: 'pie',
        data: {
            datasets: [{
                    data: [10, 20],
                    backgroundColor: [
                        'yellow',
                        'blue',
                    ]
                }, ],

            labels: ["Selesai", "Belum Selesai"]
        },
        options: {
            title: {
                display: true,
                text: 'Senarai Tempat Kediaman Beramai-ramai'
            },
            responsive: true,
            legend: {
                display: true,
                position: 'bottom',
                labels: {
                    fontSize: 6,
                    usePointStyle: true
                },

            }
        }
    });
    
});
