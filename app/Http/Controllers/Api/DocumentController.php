<?php

namespace App\Http\Controllers\Api;

use Freshbitsweb\Laratables\Laratables;

class DocumentController extends \App\Http\Controllers\Controller
{

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return Laratables::recordsOf(\App\User::class);

    }

}
