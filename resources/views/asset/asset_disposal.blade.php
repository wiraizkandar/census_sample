@extends('master')
@section('content')

<div class="container-fluid" style="background-color: white;">
    <div style="font-size: 24px;font-weight: bold;">Perlupusan Aset</div><br>
    
    <table class="table table-responsive-sm">
        <thead>
            <tr style="background-color: #43B6D7;">
                <th>No Aset</th>
                <th>Nama Aset</th>
                <th>Tindakan</th>
            </tr>
        </thead>
        <tbody>
            <tr>
                <td>1</td>
                <td>Contoh Aset</td>
                <td>
                    <a href="asset_disposal/edit" style="background-color: limegreen; border-color: limegreen;" class="btn btn-primary" role="button">
                        <i class="fa fa-pencil"></i>
                    </a>
                </td>
            </tr>
            <tr>
                <td>2</td>
                <td>Contoh Aset</td>
                <td>
                    <a href="asset_disposal/edit" style="background-color: limegreen; border-color: limegreen;" class="btn btn-primary" role="button">
                        <i class="fa fa-pencil"></i>
                    </a>
                </td>
            </tr>
            <tr>
                <td>3</td>
                <td>Contoh Aset</td>
                <td>
                    <a href="asset_disposal/edit" style="background-color: limegreen; border-color: limegreen;" class="btn btn-primary" role="button">
                        <i class="fa fa-pencil"></i>
                    </a>
                </td>
            </tr>
            <tr>
                <td>3a</td>
                <td>Contoh Aset</td>
                <td>
                    <a href="asset_disposal/edit" style="background-color: limegreen; border-color: limegreen;" class="btn btn-primary" role="button">
                        <i class="fa fa-pencil"></i>
                    </a>
                </td>
            </tr>
            <tr>
                <td>5</td>
                <td>Contoh Aset</td>
                <td>
                    <a href="asset_disposal/edit" style="background-color: limegreen; border-color: limegreen;" class="btn btn-primary" role="button">
                        <i class="fa fa-pencil"></i>
                    </a>
                </td>
            </tr>
        </tbody>
    </table>
    <ul class="pagination">
        <li class="page-item">
            <a class="page-link" href="#">Prev</a>
        </li>
        <li class="page-item active">
            <a class="page-link" href="#">1</a>
        </li>
        <li class="page-item">
            <a class="page-link" href="#">2</a>
        </li>
        <li class="page-item">
            <a class="page-link" href="#">3</a>
        </li>
        <li class="page-item">
            <a class="page-link" href="#">4</a>
        </li>
        <li class="page-item">
            <a class="page-link" href="#">Next</a>
        </li>
    </ul>
</div>
@endsection
<!-- /.conainer-fluid -->

