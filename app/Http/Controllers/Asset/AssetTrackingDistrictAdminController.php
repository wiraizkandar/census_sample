<?php

namespace App\Http\Controllers\Asset;

class AssetTrackingDistrictAdminController extends \App\Http\Controllers\Controller {
	/**
	 * Create a new controller instance.
	 *
	 * @return void
	 */
	public function __construct() {
	}

	/**
	 * Show the application dashboard.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function index() {
		return view('asset.asset_tracking_district_admin');
	}

	/**
	 * Create the application dashboard.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function create() {
		return view('asset.asset_tracking_district_admin_create');
	}

	/**
	 * Edit the application dashboard.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function edit() {
		return view('asset.asset_tracking_district_admin_edit');
	}
}
