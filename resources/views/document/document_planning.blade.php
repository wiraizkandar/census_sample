@extends('master')
@section('content')

<div class="container-fluid">
          <div class="animated fadeIn">
            <div class="row">
              <div class="col-lg-2">
                <div class="card">
                    <div class="card-body">JOHOR</div>
                </div>
                <div class="card">
                    <div class="card-body">4,000,000 Penduduk</div>
                </div>
                <div class="card">
                    <div class="card-body">10 Jumlah Daerah Pentadbiran</div>
                </div>
                <div class="card">
                    <div class="card-body">93 Jumlah Daerah Banci</div>
                </div>
                <div class="card">
                    <div class="card-body">1,0095 Jumlah Blok Perhitungan</div>
                </div>
              </div>
              <!-- /.col-->
              <div class="col-lg-10">

                <div class="card">
                  <div class="card-header">
                    <i class="fa fa-align-justify"></i> Perancangan Dokumen</div>
                  <div class="card-body">
                    <div class="form-group row">
                        <label class="col-md-2 col-form-label" for="select3">No Kod</label>
                        <div class="col-md-3">
                          <select class="form-control form-control-sm" id="select3" name="select3">
                            <option value="0">Sila pilih</option>
                            <option value="1">Option #1</option>
                            <option value="2">Option #2</option>
                            <option value="3">Option #3</option>
                          </select>
                        </div>
                        <div class="col-md-1">
                            <!-- <input class="form-control" id="cvv" type="text" placeholder="1"> -->
                        </div>
                        <label class="col-md-2 col-form-label" for="select3">Jenis Dokumen</label>
                        <div class="col-md-3">
                          <select class="form-control form-control-sm" id="select3" name="select3">
                            <option value="0">Sila pilih</option>
                            <option value="1">Option #1</option>
                            <option value="2">Option #2</option>
                            <option value="3">Option #3</option>
                          </select>
                        </div>
                      </div>
                      <div class="form-group row">
                        <label class="col-md-2 col-form-label" for="select3">Negeri</label>
                        <div class="col-md-3">
                          <select class="form-control form-control-sm" id="select3" name="select3">
                            <option value="0">Johor</option>
                            <option value="1">Option #1</option>
                            <option value="2">Option #2</option>
                            <option value="3">Option #3</option>
                          </select>
                        </div>
                        <div class="col-md-1">
                            <input class="form-control" id="cvv" type="text" placeholder="01">
                        </div>
                        <label class="col-md-2 col-form-label" for="select3">Daerah Pentadbiran</label>
                        <div class="col-md-3">
                          <select class="form-control form-control-sm" id="select3" name="select3">
                            <option value="0">Sila pilih</option>
                            <option value="1">Option #1</option>
                            <option value="2">Option #2</option>
                            <option value="3">Option #3</option>
                          </select>
                        </div>
                        <div class="col-md-1">
                            <input class="form-control" id="cvv" type="text" placeholder="">
                        </div>
                      </div>
                      <div class="form-group row">
                        <label class="col-md-2 col-form-label" for="select3">Daerah Banci</label>
                        <div class="col-md-3">
                          <select class="form-control form-control-sm" id="select3" name="select3">
                            <option value="0">Sila pilih</option>
                            <option value="1">Option #1</option>
                            <option value="2">Option #2</option>
                            <option value="3">Option #3</option>
                          </select>
                        </div>
                        <div class="col-md-1">
                            <input class="form-control" id="cvv" type="text" placeholder="">
                        </div>
                        <label class="col-md-2 col-form-label" for="select3">Blok Perhitungan</label>
                        <div class="col-md-3">
                          <select class="form-control form-control-sm" id="select3" name="select3">
                            <option value="0">Sila pilih</option>
                            <option value="1">Option #1</option>
                            <option value="2">Option #2</option>
                            <option value="3">Option #3</option>
                          </select>
                        </div>
                        <div class="col-md-1">
                            <input class="form-control" id="cvv" type="text" placeholder="">
                        </div>
                      </div>
                    <table class="table table-responsive-sm table-striped">
                      <thead>
                        <tr>
                          <th>No</th>
                          <th>No Kod</th>
                          <th>Dokumen</th>
                          <th>Keseluruhan</th>
                          <th>Negeri</th>
                          <th>Daerah Pentadbiran</th>
                          <th>Daerah Banci</th>
                          <th>Blok Perhitungan</th>
                        </tr>
                      </thead>
                      <tbody>
                        <tr>
                          <td>1</td>
                          <td>1</td>
                          <td>Buku Penyenaraian Tempat Kediaman</td>
                          <td>30,000,000</td>
                          <td>4,000,000</td>
                          <td>-</td>
                          <td>-</td>
                          <td>-</td>
                        </tr>
                        <tr>
                          <td>2</td>
                          <td>2</td>
                          <td>Soal Selidik</td>
                          <td>30,000,000</td>
                          <td>4,000,000</td>
                          <td>-</td>
                          <td>-</td>
                          <td>-</td>
                        </tr>
                        <tr>
                          <td>3</td>
                          <td>3a</td>
                          <td>Soal Selidik Tempat Kediaman Beramai-ramai(TK dan IR)</td>
                          <td>30,000,000</td>
                          <td>4,000,000</td>
                          <td>-</td>
                          <td>-</td>
                          <td>-</td>
                        </tr>
                        <tr>
                          <td>3</td>
                          <td>3b</td>
                          <td>Soal Selidik Tempat Kediaman Beramai-ramai(Perseorangan)</td>
                          <td>20,000,000</td>
                          <td>4,000,000</td>
                          <td>-</td>
                          <td>-</td>
                          <td>-</td>
                        </tr>
                        <tr>
                          <td>5</td>
                          <td>4</td>
                          <td>Kad Kod</td>
                          <td>1,000,000</td>
                          <td>4,000,000</td>
                          <td>-</td>
                          <td>-</td>
                          <td>-</td>
                        </tr>
                        <tr>
                          <td>6</td>
                          <td>5</td>
                          <td>Kad Penyenaraian Tempat Kediaman</td>
                          <td>4,000,000</td>
                          <td>4,000,000</td>
                          <td>-</td>
                          <td>-</td>
                          <td>-</td>
                        </tr>
                        <tr>
                          <td>7</td>
                          <td>6</td>
                          <td>Panduan Mengisi e-Census</td>
                          <td>4,000,000</td>
                          <td>4,000,000</td>
                          <td>-</td>
                          <td>-</td>
                          <td>-</td>
                        </tr>
                        <tr>
                          <td>8</td>
                          <td>7</td>
                          <td>Surat Lawatan Semula</td>
                          <td>2,000,000</td>
                          <td>4,000,000</td>
                          <td>-</td>
                          <td>-</td>
                          <td>-</td>
                        </tr>
                        <tr>
                          <td>9</td>
                          <td>8</td>
                          <td>Label Kotak Blok Penghitungan</td>
                          <td>10,000,000</td>
                          <td>4,000,000</td>
                          <td>-</td>
                          <td>-</td>
                          <td>-</td>
                        </tr>
                        <tr>
                          <td>5</td>
                          <td>4</td>
                          <td>Borang Kawalan e-Census</td>
                          <td>4,000,000</td>
                          <td>4,000,000</td>
                          <td>-</td>
                          <td>-</td>
                          <td>-</td>
                        </tr>

                      </tbody>
                    </table>
                    <ul class="pagination">
                      <li class="page-item">
                        <a class="page-link" href="#">Prev</a>
                      </li>
                      <li class="page-item active">
                        <a class="page-link" href="#">1</a>
                      </li>
                      <li class="page-item">
                        <a class="page-link" href="#">2</a>
                      </li>
                      <li class="page-item">
                        <a class="page-link" href="#">3</a>
                      </li>
                      <li class="page-item">
                        <a class="page-link" href="#">4</a>
                      </li>
                      <li class="page-item">
                        <a class="page-link" href="#">Next</a>
                      </li>
                    </ul>
                  </div>
                </div>
              </div>
              <!-- /.col-->
            </div>
@endsection
<!-- /.conainer-fluid -->

