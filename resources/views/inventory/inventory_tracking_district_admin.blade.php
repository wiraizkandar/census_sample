@extends('master')
@section('content')

<div class="container-fluid">

    <div class="animated fadeIn">
        <div class="row">
            <div class="col-sm-8">
<!--                 <div class="row">
                    <canvas id="inventory_statistic_by_state"  height="140"></canvas>
                    <button style="display: none" id="inventory_statistic_by_district_btn"  class="btn btn-primary" >Kembali</button>
                    <canvas style="display: none" id="inventory_statistic_by_district"  height="140"></canvas>
                </div> -->
                <div class="card">
                    <div class="card-body">
                        <div class="row">
                            <label class="col-md-2 col-form-label" for="select3">Fasa Pembancian</label>
                            <div class="col-md-4">
                              <select class="form-control form-control-sm" id="select3" name="select3">
                                <option value="0">Pra Banci</option>
                                <option value="1">Option #1</option>
                                <option value="2">Option #2</option>
                                <option value="3">Option #3</option>
                              </select>
                            </div>
                        </div>
                        <br>
                        <!-- <div class="card">
                            <div class="card-body"> -->
                        <div class="row">
                            <label style="color:blue"><b>Negeri : 01 > &nbsp;</b></label>
                            <label><b><u>Daerah Pentadbiran</u></b></label>
                        </div>
                        <div class="row">

                            <table class="table table-responsive-sm table-sm">
                              <thead>
                                <tr>
                                  <th>Kod</th>
                                  <th>Daerah</th>
                                  <th>Perancangan Agihan Kuantiti</th>
                                  <th>Diterima</th>
                                  <th>Baki Belum Terima</th>
                                  <th>Tahap</th>
                                </tr>
                              </thead>
                              <tbody>
                                <tr>
                                  <td>01</td>
                                  <td><u>Batu Pahat</u></td>
                                  <td><center>10,000</td>
                                  <td></td>
                                  <td></td>
                                  <td></td>
                                </tr>
                                <tr>
                                  <td>02</td>
                                  <td>Johor Bahru</td>
                                  <td><center>4,000</td>
                                  <td></td>
                                  <td></td>
                                  <td></td>
                                </tr>
                                <tr>
                                  <td>03</td>
                                  <td>Kluang</td>
                                  <td><center>6,000</td>
                                  <td></td>
                                  <td></td>
                                  <td></td>
                                </tr>
                                <tr>
                                  <td>04</td>
                                  <td>Kota Tinggi</td>
                                  <td><center>3,000</td>
                                  <td></td>
                                  <td></td>
                                  <td></td>
                                </tr>
                                <tr>
                                  <td>05</td>
                                  <td>Mersing</td>
                                  <td><center>6,000</td>
                                  <td></td>
                                  <td></td>
                                  <td></td>
                                </tr>
                                <tr>
                                  <td>06</td>
                                  <td>Muar</td>
                                  <td><center>8,000</td>
                                  <td></td>
                                  <td></td>
                                  <td></td>
                                </tr>
                                <tr>
                                  <td>07</td>
                                  <td>Pontian</td>
                                  <td><center>4,000</td>
                                  <td></td>
                                  <td></td>
                                  <td></td>
                                </tr>
                                <tr>
                                  <td>08</td>
                                  <td>Segamat</td>
                                  <td><center>7,000</td>
                                  <td></td>
                                  <td></td>
                                  <td></td>
                                </tr>
                                <tr>
                                  <td>21</td>
                                  <td>Kulai</td>
                                  <td><center>3,000</td>
                                  <td></td>
                                  <td></td>
                                  <td></td>
                                </tr>
                                <tr>
                                  <td>22</td>
                                  <td>Tangkak</td>
                                  <td><center>12,000</td>
                                  <td></td>
                                  <td></td>
                                  <td></td>
                                </tr>
                              </tbody>
                            </table>
                        <!-- </div> -->
                        </div>
                        <div class="row">
                            <label style="margin-left: 500px;">Maklumat</label>
                        </div>
                        <div class="row">
                            <button class="btn btn-sm btn-secondary" type="submit" style="margin-left: 400px; min-width: 50px; max-height: 20px"></button>
                            <label>&nbsp;BELUM SELESAI</label>
                        </div>
                        <div class="row">
                            <button class="btn btn-sm btn-primary" type="submit" style="margin-left: 400px; min-width: 50px; max-height: 20px"></button>
                            <label>&nbsp;JKK</label>
                        </div>
                        <div class="row">
                            <button class="btn btn-sm btn-warning" type="submit" style="margin-left: 400px; min-width: 50px; max-height: 20px"></button>
                            <label>&nbsp;NEGERI</label>
                        </div>
                        <div class="row">
                            <button class="btn btn-sm btn-danger" type="submit" style="margin-left: 400px; min-width: 50px; max-height: 20px"></button>
                            <label>&nbsp;PENGUASA DAERAH</label>
                        </div>
                        <div class="row">
                            <button class="btn btn-sm btn-dark" type="submit" style="margin-left: 400px; min-width: 50px; max-height: 20px"></button>
                            <label>&nbsp;PENYELIA</label>
                        </div>
                        <div class="row">
                            <button class="btn btn-sm btn-success" type="submit" style="margin-left: 400px; min-width: 50px; max-height: 20px"></button>
                            <label>&nbsp;PEMBANCI</label>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-sm-4">
                <div class="card">
                    <div class="card-body">
                        <div class="row">
                            <label><u><center>Ringkasan status penerimaan dokumen peringkat daerah pentadbiran</center></u></label>
                        </div>
                        <div class="row">
                            <div class="col-sm-3">
                                <div class="row">
                                    <label>Batu Pahat</label>
                                </div>
                            </div>
                            <div class="col-sm-2">
                                <div class="row">
                                    <button class="btn btn-sm btn-secondary" type="submit" style="min-width: 50px; max-height: 20px">0%</button>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-3">
                                <div class="row">
                                    <label>Johor Bahru</label>
                                </div>
                            </div>
                            <div class="col-sm-2">
                                <div class="row">
                                    <button class="btn btn-sm btn-secondary" type="submit" style="min-width: 50px; max-height: 20px">0%</button>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-3">
                                <div class="row">
                                    <label>Kluang</label>
                                </div>
                            </div>
                            <div class="col-sm-2">
                                <div class="row">
                                    <button class="btn btn-sm btn-secondary" type="submit" style="min-width: 50px; max-height: 20px">0%</button>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-3">
                                <div class="row">
                                    <label>Kota Tinggi</label>
                                </div>
                            </div>
                            <div class="col-sm-2">
                                <div class="row">
                                    <button class="btn btn-sm btn-secondary" type="submit" style="min-width: 50px; max-height: 20px">0%</button>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-3">
                                <div class="row">
                                    <label>Mersing</label>
                                </div>
                            </div>
                            <div class="col-sm-2">
                                <div class="row">
                                    <button class="btn btn-sm btn-secondary" type="submit" style="min-width: 50px; max-height: 20px">0%</button>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-3">
                                <div class="row">
                                    <label>Muar</label>
                                </div>
                            </div>
                            <div class="col-sm-2">
                                <div class="row">
                                    <button class="btn btn-sm btn-secondary" type="submit" style="min-width: 50px; max-height: 20px">0%</button>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-3">
                                <div class="row">
                                    <label>Pontian</label>
                                </div>
                            </div>
                            <div class="col-sm-2">
                                <div class="row">
                                    <button class="btn btn-sm btn-secondary" type="submit" style="min-width: 50px; max-height: 20px">0%</button>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-3">
                                <div class="row">
                                    <label>Segamat</label>
                                </div>
                            </div>
                            <div class="col-sm-2">
                                <div class="row">
                                    <button class="btn btn-sm btn-secondary" type="submit" style="min-width: 50px; max-height: 20px">0%</button>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-3">
                                <div class="row">
                                    <label>Kulai</label>
                                </div>
                            </div>
                            <div class="col-sm-2">
                                <div class="row">
                                    <button class="btn btn-sm btn-secondary" type="submit" style="min-width: 50px; max-height: 20px">0%</button>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-3">
                                <div class="row">
                                    <label>Tangkak</label>
                                </div>
                            </div>
                            <div class="col-sm-2">
                                <div class="row">
                                    <button class="btn btn-sm btn-secondary" type="submit" style="min-width: 50px; max-height: 20px">0%</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    <!--/.row-->
</div>

</div>
@endsection
<!-- /.conainer-fluid -->

@section('myscript')
@endsection