<?php

namespace App\Http\Controllers\Vendor;

class VendorManagementController extends \App\Http\Controllers\Controller {
	/**
	 * Create a new controller instance.
	 *
	 * @return void
	 */
	public function __construct() {
	}

	/**
	 * Show the application dashboard.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function index() {
		return view('vendor.vendor_management');
	}

	/**
	 * Create the application dashboard.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function create() {
		return view('vendor.vendor_management_create');
	}

	/**
	 * Edit the application dashboard.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function edit() {
		return view('vendor.vendor_management_edit');
	}
}
