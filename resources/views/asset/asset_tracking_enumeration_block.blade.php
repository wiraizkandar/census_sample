@extends('master')
@section('content')

<div class="container-fluid">

    <div class="animated fadeIn">
        <div class="row">
            <div class="col-sm-8">
<!--                 <div class="row">
                    <canvas id="asset_statistic_by_state"  height="140"></canvas>
                    <button style="display: none" id="asset_statistic_by_district_btn"  class="btn btn-primary" >Kembali</button>
                    <canvas style="display: none" id="asset_statistic_by_district"  height="140"></canvas>
                </div> -->
                <div class="card">
                    <div class="card-body">
                        <div class="row">
                            <label class="col-md-2 col-form-label" for="select3">Mod</label>
                            <div class="col-md-4">
                              <select class="form-control form-control-sm" id="select3" name="select3">
                                <option value="0">Pre Census</option>
                                <option value="1">Option #1</option>
                                <option value="2">Option #2</option>
                                <option value="3">Option #3</option>
                              </select>
                            </div>
                        </div>
                        <br>
                        <!-- <div class="card">
                            <div class="card-body"> -->
                        <div class="row">
                            <label style="color:blue"><b>Negeri : 01 > &nbsp; Daerah Pentabiran : 01 > Daerah Banci : 001 >&nbsp;</b></label>
                            <label><b><u>Blok Perhitungan</u></b></label>
                        </div>
                        <div class="row">

                            <table class="table table-responsive-sm table-sm">
                              <thead>
                                <tr>
                                  <th>No Blok Perhitungan</th>
                                  <th>Perancangan Agihan Kuantiti</th>
                                  <th>Diterima</th>
                                  <th>Baki Belum Terima</th>
                                  <th>Tahap</th>
                                </tr>
                              </thead>
                              <tbody>
                                <tr>
                                  <td>001</td>
                                  <td><center>900</td>
                                  <td></td>
                                  <td></td>
                                  <td></td>
                                </tr>
                                <tr>
                                  <td>002</td>
                                  <td><center>200</td>
                                  <td></td>
                                  <td></td>
                                  <td></td>
                                </tr>
                                <tr>
                                  <td>003</td>
                                  <td><center>300</td>
                                  <td></td>
                                  <td></td>
                                  <td></td>
                                </tr>
                                <tr>
                                  <td>004</td>
                                  <td><center>280</td>
                                  <td></td>
                                  <td></td>
                                  <td></td>
                                </tr>
                                <tr>
                                  <td>005</td>
                                  <td><center>320</td>
                                  <td></td>
                                  <td></td>
                                  <td></td>
                                </tr>
                                <tr>
                                  <td>006</td>
                                  <td><center>400</td>
                                  <td></td>
                                  <td></td>
                                  <td></td>
                                </tr>
                                <tr>
                                  <td>007</td>
                                  <td><center>270</td>
                                  <td></td>
                                  <td></td>
                                  <td></td>
                                </tr>
                                <tr>
                                  <td>008</td>
                                  <td><center>410</td>
                                  <td></td>
                                  <td></td>
                                  <td></td>
                                </tr>
                                <tr>
                                  <td>009</td>
                                  <td><center>210</td>
                                  <td></td>
                                  <td></td>
                                  <td></td>
                                </tr>
                                <tr>
                                  <td>010</td>
                                  <td><center>1300</td>
                                  <td></td>
                                  <td></td>
                                  <td></td>
                                </tr>
                              </tbody>
                            </table>
                        <!-- </div> -->
                        </div>
                        <div class="row">
                            <label style="margin-left: 500px;">Maklumat</label>
                        </div>
                        <div class="row">
                            <button class="btn btn-sm btn-secondary" type="submit" style="margin-left: 400px; min-width: 50px; max-height: 20px"></button>
                            <label>&nbsp;BELUM SELESAI</label>
                        </div>
                        <div class="row">
                            <button class="btn btn-sm btn-primary" type="submit" style="margin-left: 400px; min-width: 50px; max-height: 20px"></button>
                            <label>&nbsp;JKK</label>
                        </div>
                        <div class="row">
                            <button class="btn btn-sm btn-warning" type="submit" style="margin-left: 400px; min-width: 50px; max-height: 20px"></button>
                            <label>&nbsp;NEGERI</label>
                        </div>
                        <div class="row">
                            <button class="btn btn-sm btn-danger" type="submit" style="margin-left: 400px; min-width: 50px; max-height: 20px"></button>
                            <label>&nbsp;PENGUASA DAERAH</label>
                        </div>
                        <div class="row">
                            <button class="btn btn-sm btn-dark" type="submit" style="margin-left: 400px; min-width: 50px; max-height: 20px"></button>
                            <label>&nbsp;PENYELIA</label>
                        </div>
                        <div class="row">
                            <button class="btn btn-sm btn-success" type="submit" style="margin-left: 400px; min-width: 50px; max-height: 20px"></button>
                            <label>&nbsp;PEMBANCI</label>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-sm-4">
                <div class="card">
                    <div class="card-body">
                        <div class="row">
                            <label><u><center>Ringkasan status penerimaan dokumen peringkat daerah banci</center></u></label>
                        </div>
                        <div class="row">
                            <div class="col-sm-3">
                                <div class="row">
                                    <label>001</label>
                                </div>
                            </div>
                            <div class="col-sm-2">
                                <div class="row">
                                    <button class="btn btn-sm btn-secondary" type="submit" style="min-width: 50px; max-height: 20px">0%</button>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-3">
                                <div class="row">
                                    <label>002</label>
                                </div>
                            </div>
                            <div class="col-sm-2">
                                <div class="row">
                                    <button class="btn btn-sm btn-secondary" type="submit" style="min-width: 50px; max-height: 20px">0%</button>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-3">
                                <div class="row">
                                    <label>003</label>
                                </div>
                            </div>
                            <div class="col-sm-2">
                                <div class="row">
                                    <button class="btn btn-sm btn-secondary" type="submit" style="min-width: 50px; max-height: 20px">0%</button>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-3">
                                <div class="row">
                                    <label>004</label>
                                </div>
                            </div>
                            <div class="col-sm-2">
                                <div class="row">
                                    <button class="btn btn-sm btn-secondary" type="submit" style="min-width: 50px; max-height: 20px">0%</button>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-3">
                                <div class="row">
                                    <label>005</label>
                                </div>
                            </div>
                            <div class="col-sm-2">
                                <div class="row">
                                    <button class="btn btn-sm btn-secondary" type="submit" style="min-width: 50px; max-height: 20px">0%</button>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-3">
                                <div class="row">
                                    <label>006</label>
                                </div>
                            </div>
                            <div class="col-sm-2">
                                <div class="row">
                                    <button class="btn btn-sm btn-secondary" type="submit" style="min-width: 50px; max-height: 20px">0%</button>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-3">
                                <div class="row">
                                    <label>007</label>
                                </div>
                            </div>
                            <div class="col-sm-2">
                                <div class="row">
                                    <button class="btn btn-sm btn-secondary" type="submit" style="min-width: 50px; max-height: 20px">0%</button>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-3">
                                <div class="row">
                                    <label>008</label>
                                </div>
                            </div>
                            <div class="col-sm-2">
                                <div class="row">
                                    <button class="btn btn-sm btn-secondary" type="submit" style="min-width: 50px; max-height: 20px">0%</button>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-3">
                                <div class="row">
                                    <label>009</label>
                                </div>
                            </div>
                            <div class="col-sm-2">
                                <div class="row">
                                    <button class="btn btn-sm btn-secondary" type="submit" style="min-width: 50px; max-height: 20px">0%</button>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-3">
                                <div class="row">
                                    <label>010</label>
                                </div>
                            </div>
                            <div class="col-sm-2">
                                <div class="row">
                                    <button class="btn btn-sm btn-secondary" type="submit" style="min-width: 50px; max-height: 20px">0%</button>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-3">
                                <div class="row">
                                    <label>011</label>
                                </div>
                            </div>
                            <div class="col-sm-2">
                                <div class="row">
                                    <button class="btn btn-sm btn-secondary" type="submit" style="min-width: 50px; max-height: 20px">0%</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    <!--/.row-->
</div>

</div>
@endsection
<!-- /.conainer-fluid -->

@section('myscript')
@endsection