<?php

namespace App\Http\Controllers\Document;

class DocumentStoreRecordController extends \App\Http\Controllers\Controller
{

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        
    }



    /**
     * Create the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('document.document_store_record_create');
    }



}
