<?php

namespace App\Http\Controllers\Document;

class DocumentDeliveryPostCensusController extends \App\Http\Controllers\Controller {
	/**
	 * Create a new controller instance.
	 *
	 * @return void
	 */
	public function __construct() {
	}

	/**
	 * Show the application dashboard.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function create() {
		return view('document.document_delivery_post_census_create');
	}
}
