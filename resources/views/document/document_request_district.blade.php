@extends('master')
@section('content')

<div class="container-fluid">
          <div class="animated fadeIn">
            <div class="col-lg-4">
              <div class="row">
                 <div class="input-group">
                  <input class="form-control" id="input1-group2" type="text" name="input1-group2" placeholder="Cari Nama Dokumen">
                  <span class="input-group-prepend">
                    <button class="btn btn-primary" type="button">
                      <i class="fa fa-search"></i></button>
                  </span>
                </div>
              </div>
              <div class="col-lg-12">
                <div class="row">
                  <center>Maklumat Baki Dokumen Setiap Daerah</center>
                </div>
              </div>
            </div>
            <br>
            <div class="row">
              <div class="col-lg-2">
                <div class="card">
                    <div class="card-body">J.BAHRU 0</div>
                </div>
                <div class="card">
                    <div class="card-body">KLUANG 0</div>
                </div>
                <div class="card">
                    <div class="card-body">MUAR 0</div>
                </div>
                <div class="card">
                    <div class="card-body">SEGAMAT 0</div>
                </div>
                <div class="card">
                    <div class="card-body">TANGKAK 0</div>
                </div>
              </div>
              <div class="col-lg-2">
                <div class="card">
                    <div class="card-body">B.PAHAT 0</div>
                </div>
                <div class="card">
                    <div class="card-body">KULAI 0</div>
                </div>
                <div class="card">
                    <div class="card-body">K.TINGGI 0</div>
                </div>
                <div class="card">
                    <div class="card-body">PONTIAN 0</div>
                </div>
                <div class="card">
                    <div class="card-body">MERSING 0</div>
                </div>
              </div>
              <!-- /.col-->
              <div class="col-lg-8">

                <div class="card">
                  <div class="card-header">
                    <i class="fa fa-align-justify"></i> Permintaan Dokumen Bagi Daerah</div>
                  <div class="card-body">
                    <div class="form-group row">
                        &nbsp;&nbsp;&nbsp;
                        <!-- <button href="/document_request/create" class="col-md-2 btn btn-sm btn-primary" type="submit">Permintaan</button> -->
                        <a class="col-md-2 btn btn-sm btn-primary" href="document_request/create" role="button">Permintaan</a>
                      </div>
                    <table class="table table-responsive-sm table-sm">
                      <thead>
                        <tr>
                          <th>No</th>
                          <th>Tarikh Permintaan</th>
                          <th>Kod Dokumen</th>
                          <th>Nama Dokumen</th>
                          <th>Negeri Pemohon</th>
                          <th>Negeri Dipohon</th>
                          <th>Kuantiti</th>
                          <th>Status</th>
                          <th>Tindakan</th>
                        </tr>
                      </thead>
                      <tbody>
                        <tr>
                          <td>1</td>
                          <td>30/08/2018</td>
                          <td>2</td>
                          <td>Soal Selidik</td>
                          <td>Johor Bahru</td>
                          <td>Alor Gajah</td>
                          <td>100</td>
                          <td><button class="btn btn-sm btn-primary" type="submit">Lulus</button></td>
                          <td>
                            <!-- <span class="badge badge-success">Active</span> -->
                            <button class="btn btn-sm btn-success" type="submit">
                      <i class="fa fa-dot-circle-o"></i></button>
                      <button class="btn btn-sm btn-danger" type="submit">
                      <i class="fa fa-dot-circle-o"></i></button>
                          </td>
                        </tr>
                        <tr>
                          <td></td>
                          <td></td>
                          <td></td>
                          <td></td>
                          <td></td>
                          <td></td>
                          <td></td>
                          <td></td>
                          <td></td>
                        </tr>
                        <tr>
                          <td></td>
                          <td></td>
                          <td></td>
                          <td></td>
                          <td></td>
                          <td></td>
                          <td></td>
                          <td></td>
                          <td></td>
                        </tr>
                        <tr>
                          <td></td>
                          <td></td>
                          <td></td>
                          <td></td>
                          <td></td>
                          <td></td>
                          <td></td>
                          <td></td>
                          <td></td>
                        </tr>
                        <tr>
                          <td></td>
                          <td></td>
                          <td></td>
                          <td></td>
                          <td></td>
                          <td></td>
                          <td></td>
                          <td></td>
                          <td></td>
                        </tr>
                        <tr>
                          <td></td>
                          <td></td>
                          <td></td>
                          <td></td>
                          <td></td>
                          <td></td>
                          <td></td>
                          <td></td>
                          <td></td>
                        </tr>
                        <tr>
                          <td></td>
                          <td></td>
                          <td></td>
                          <td></td>
                          <td></td>
                          <td></td>
                          <td></td>
                          <td></td>
                          <td></td>
                        </tr>
                        <tr>
                          <td></td>
                          <td></td>
                          <td></td>
                          <td></td>
                          <td></td>
                          <td></td>
                          <td></td>
                          <td></td>
                          <td></td>
                        </tr>
                        <tr>
                          <td></td>
                          <td></td>
                          <td></td>
                          <td></td>
                          <td></td>
                          <td></td>
                          <td></td>
                          <td></td>
                          <td></td>
                        </tr>
                        <tr>
                          <td></td>
                          <td></td>
                          <td></td>
                          <td></td>
                          <td></td>
                          <td></td>
                          <td></td>
                          <td></td>
                          <td></td>
                        </tr>
                        <tr>
                          <td></td>
                          <td></td>
                          <td></td>
                          <td></td>
                          <td></td>
                          <td></td>
                          <td></td>
                          <td></td>
                          <td></td>
                        </tr>
                        <tr>
                          <td></td>
                          <td></td>
                          <td></td>
                          <td></td>
                          <td></td>
                          <td></td>
                          <td></td>
                          <td></td>
                          <td></td>
                        </tr>
                        <tr>
                          <td></td>
                          <td></td>
                          <td></td>
                          <td></td>
                          <td></td>
                          <td></td>
                          <td></td>
                          <td></td>
                          <td></td>
                        </tr>
                        <tr>
                          <td></td>
                          <td></td>
                          <td></td>
                          <td></td>
                          <td></td>
                          <td></td>
                          <td></td>
                          <td></td>
                          <td></td>
                        </tr>
                        <tr>
                          <td></td>
                          <td></td>
                          <td></td>
                          <td></td>
                          <td></td>
                          <td></td>
                          <td></td>
                          <td></td>
                          <td></td>
                        </tr>
                        <tr>
                          <td></td>
                          <td></td>
                          <td></td>
                          <td></td>
                          <td></td>
                          <td></td>
                          <td></td>
                          <td></td>
                          <td></td>
                        </tr>
                        <tr>
                          <td></td>
                          <td></td>
                          <td></td>
                          <td></td>
                          <td></td>
                          <td></td>
                          <td></td>
                          <td></td>
                          <td></td>
                        </tr>
                        <tr>
                          <td></td>
                          <td></td>
                          <td></td>
                          <td></td>
                          <td></td>
                          <td></td>
                          <td></td>
                          <td></td>
                          <td></td>
                        </tr>
                        <tr>
                          <td></td>
                          <td></td>
                          <td></td>
                          <td></td>
                          <td></td>
                          <td></td>
                          <td></td>
                          <td></td>
                          <td></td>
                        </tr>
                        <tr>
                          <td></td>
                          <td></td>
                          <td></td>
                          <td></td>
                          <td></td>
                          <td></td>
                          <td></td>
                          <td></td>
                          <td></td>
                        </tr>
                        <tr>
                          <td></td>
                          <td></td>
                          <td></td>
                          <td></td>
                          <td></td>
                          <td></td>
                          <td></td>
                          <td></td>
                          <td></td>
                        </tr>
                        <tr>
                          <td></td>
                          <td></td>
                          <td></td>
                          <td></td>
                          <td></td>
                          <td></td>
                          <td></td>
                          <td></td>
                          <td></td>
                        </tr>
                        <tr>
                          <td></td>
                          <td></td>
                          <td></td>
                          <td></td>
                          <td></td>
                          <td></td>
                          <td></td>
                          <td></td>
                          <td></td>
                        </tr>
                        <tr>
                          <td></td>
                          <td></td>
                          <td></td>
                          <td></td>
                          <td></td>
                          <td></td>
                          <td></td>
                          <td></td>
                          <td></td>
                        </tr>
                        <tr>
                          <td></td>
                          <td></td>
                          <td></td>
                          <td></td>
                          <td></td>
                          <td></td>
                          <td></td>
                          <td></td>
                          <td></td>
                        </tr>
                        <tr>
                          <td></td>
                          <td></td>
                          <td></td>
                          <td></td>
                          <td></td>
                          <td></td>
                          <td></td>
                          <td></td>
                          <td></td>
                        </tr>
                      </tbody>
                    </table>
                    <ul class="pagination">
                      <li class="page-item">
                        <a class="page-link" href="#">Prev</a>
                      </li>
                      <li class="page-item active">
                        <a class="page-link" href="#">1</a>
                      </li>
                      <li class="page-item">
                        <a class="page-link" href="#">2</a>
                      </li>
                      <li class="page-item">
                        <a class="page-link" href="#">3</a>
                      </li>
                      <li class="page-item">
                        <a class="page-link" href="#">4</a>
                      </li>
                      <li class="page-item">
                        <a class="page-link" href="#">Next</a>
                      </li>
                    </ul>
                  </div>
                </div>
              </div>
              <!-- /.col-->
            </div>
@endsection
<!-- /.conainer-fluid -->

