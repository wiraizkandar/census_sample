$(function () {
    'use strict';
    var ctx1 = document.getElementById("personel_stats").getContext('2d');
    var myChart1 = new Chart(ctx1, {
        type: 'bar',
        data: {
            labels: ["1", "2", "3"],
            datasets:
                    [{
                            label: 'Selesai',
                            data: [41, 22, 57],
                            backgroundColor: 'blue'
                        }
                    ]
        },
        options: {
             legend: {
                display: false,
            },
            title: {
                display: true,
                text: 'Senarai Personel Banci 2020'
            },
            scales: {
                xAxes: [{
                        ticks: {
                            autoSkip: false,
                            fontSize: 9,
                        },
                        stacked: true,
                    }],
                yAxes: [{
                        ticks: {
                            beginAtZero: true,
                            steps: 10,
                            stepValue: 5,
                            max: 100
                        },
                        stacked: true
                    }]
            },
        }
    });
});
