<?php

namespace App\Http\Controllers\Personnel;

class PersonnelRecruitmentInterviewResultController extends \App\Http\Controllers\Controller {
	/**
	 * Create a new controller instance.
	 *
	 * @return void
	 */
	public function __construct() {
	}

	/**
	 * Show the application dashboard.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function index() {
		return view('personnel.recruitment_interview_result');
	}
        
	public function edit() {
		return view('personnel.recruitment_interview_result_edit');
	}
}
