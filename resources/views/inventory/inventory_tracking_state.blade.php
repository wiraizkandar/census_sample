@extends('master')
@section('content')

<div class="container-fluid">

    <div class="animated fadeIn">
        <div class="row">
            <div class="col-sm-8">
<!--                 <div class="row">
                    <canvas id="inventory_statistic_by_state"  height="140"></canvas>
                    <button style="display: none" id="inventory_statistic_by_district_btn"  class="btn btn-primary" >Kembali</button>
                    <canvas style="display: none" id="inventory_statistic_by_district"  height="140"></canvas>
                </div> -->
                <div class="card">
                    <div class="card-body">
                        <div class="row">
                            <label class="col-md-2 col-form-label" for="select3">Nama Dokumen</label>
                            <div class="col-md-4">
                              <select class="form-control form-control-sm" id="select3" name="select3">
                                <option value="0">Soal Selidik</option>
                                <option value="1">Option #1</option>
                                <option value="2">Option #2</option>
                                <option value="3">Option #3</option>
                              </select>
                            </div>
                            <label class="col-md-2 col-form-label" for="select3">Fasa Pembancian</label>
                            <div class="col-md-4">
                              <select class="form-control form-control-sm" id="select3" name="select3">
                                <option value="0">Pra Banci</option>
                                <option value="1">Option #1</option>
                                <option value="2">Option #2</option>
                                <option value="3">Option #3</option>
                              </select>
                            </div>
                        </div>
                        <br>
                        <!-- <div class="card">
                            <div class="card-body"> -->
                        <div class="row">
                            <label><b>Negeri ></b></label>
                        </div>
                        <div class="row">

                            <table class="table table-responsive-sm table-sm">
                              <thead>
                                <tr>
                                  <th>Kod</th>
                                  <th>Negeri</th>
                                  <th>Perancangan Agihan Kuantiti</th>
                                  <th>Diterima</th>
                                  <th>Baki Belum Terima</th>
                                  <th>Tahap</th>
                                  <th>Tindakan</th>
                                </tr>
                              </thead>
                              <tbody>
                                <tr>
                                  <td>01</td>
                                  <td>Johor</td>
                                  <td><center>100,000</td>
                                  <td>10,000</td>
                                  <td>0</td>
                                  <td><button class="btn btn-sm btn-warning" type="submit"></button></td>
                                  <td><button class="btn btn-sm btn-primary" type="submit"></button></td>
                                </tr>
                                <tr>
                                  <td>02</td>
                                  <td>Kedah</td>
                                  <td><center>40,000</center></td>
                                  <td></td>
                                  <td></td>
                                  <td></td>
                                  <td></td>
                                </tr>
                                <tr>
                                  <td>03</td>
                                  <td>Kelantan</td>
                                  <td><center>60,000</center></td>
                                  <td></td>
                                  <td></td>
                                  <td></td>
                                  <td></td>
                                </tr>
                                <tr>
                                  <td>04</td>
                                  <td>Melaka</td>
                                  <td><center>30,000</center></td>
                                  <td></td>
                                  <td></td>
                                  <td></td>
                                  <td></td>
                                </tr>
                                <tr>
                                  <td>05</td>
                                  <td>N.Sembilan</td>
                                  <td><center>60,000</center></td>
                                  <td></td>
                                  <td></td>
                                  <td></td>
                                  <td></td>
                                </tr>
                                <tr>
                                  <td>06</td>
                                  <td>Pahang</td>
                                  <td><center>80,000</center></td>
                                  <td></td>
                                  <td></td>
                                  <td></td>
                                  <td></td>
                                </tr>
                                <tr>
                                  <td>07</td>
                                  <td>P.Pinang</td>
                                  <td><center>40,000</center></td>
                                  <td></td>
                                  <td></td>
                                  <td></td>
                                  <td></td>
                                </tr>
                                <tr>
                                  <td>08</td>
                                  <td>Perak</td>
                                  <td><center>70,000</center></td>
                                  <td></td>
                                  <td></td>
                                  <td></td>
                                  <td></td>
                                </tr>
                                <tr>
                                  <td>09</td>
                                  <td>Perlis</td>
                                  <td><center>30,000</center></td>
                                  <td></td>
                                  <td></td>
                                  <td></td>
                                  <td></td>
                                </tr>
                                <tr>
                                  <td>10</td>
                                  <td>Selangor</td>
                                  <td><center>120,000</center></td>
                                  <td></td>
                                  <td></td>
                                  <td></td>
                                  <td></td>
                                </tr>
                              </tbody>
                            </table>
                        <!-- </div> -->
                        </div>
                        <div class="row">
                            <label style="margin-left: 500px;">Maklumat</label>
                        </div>
                        <div class="row">
                            <button class="btn btn-sm btn-secondary" type="submit" style="margin-left: 400px; min-width: 50px; max-height: 20px"></button>
                            <label>&nbsp;BELUM SELESAI</label>
                        </div>
                        <div class="row">
                            <button class="btn btn-sm btn-primary" type="submit" style="margin-left: 400px; min-width: 50px; max-height: 20px"></button>
                            <label>&nbsp;JKK</label>
                        </div>
                        <div class="row">
                            <button class="btn btn-sm btn-warning" type="submit" style="margin-left: 400px; min-width: 50px; max-height: 20px"></button>
                            <label>&nbsp;NEGERI</label>
                        </div>
                        <div class="row">
                            <button class="btn btn-sm btn-danger" type="submit" style="margin-left: 400px; min-width: 50px; max-height: 20px"></button>
                            <label>&nbsp;PENGUASA DAERAH</label>
                        </div>
                        <div class="row">
                            <button class="btn btn-sm btn-dark" type="submit" style="margin-left: 400px; min-width: 50px; max-height: 20px"></button>
                            <label>&nbsp;PENYELIA</label>
                        </div>
                        <div class="row">
                            <button class="btn btn-sm btn-success" type="submit" style="margin-left: 400px; min-width: 50px; max-height: 20px"></button>
                            <label>&nbsp;PEMBANCI</label>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-sm-4">
                <div class="card">
                    <div class="card-body">
                        <div class="row">
                            <label><u><center>Ringkasan status penerimaan dokumen peringkat negeri</center></u></label>
                        </div>
                        <div class="row">
                            <div class="col-sm-3">
                                <div class="row">
                                    <label>Johor</label>
                                </div>
                            </div>
                            <div class="col-sm-2">
                                <div class="row">
                                    <button class="btn btn-sm btn-warning" type="submit" style="min-width: 200px; max-height: 20px">100%</button>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-3">
                                <div class="row">
                                    <label>Kedah</label>
                                </div>
                            </div>
                            <div class="col-sm-2">
                                <div class="row">
                                    <button class="btn btn-sm btn-secondary" type="submit" style="min-width: 50px; max-height: 20px">0%</button>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-3">
                                <div class="row">
                                    <label>Kelantan</label>
                                </div>
                            </div>
                            <div class="col-sm-2">
                                <div class="row">
                                    <button class="btn btn-sm btn-secondary" type="submit" style="min-width: 50px; max-height: 20px">0%</button>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-3">
                                <div class="row">
                                    <label>Melaka</label>
                                </div>
                            </div>
                            <div class="col-sm-2">
                                <div class="row">
                                    <button class="btn btn-sm btn-secondary" type="submit" style="min-width: 50px; max-height: 20px">0%</button>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-3">
                                <div class="row">
                                    <label>N.Sembilan</label>
                                </div>
                            </div>
                            <div class="col-sm-2">
                                <div class="row">
                                    <button class="btn btn-sm btn-secondary" type="submit" style="min-width: 50px; max-height: 20px">0%</button>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-3">
                                <div class="row">
                                    <label>Pahang</label>
                                </div>
                            </div>
                            <div class="col-sm-2">
                                <div class="row">
                                    <button class="btn btn-sm btn-secondary" type="submit" style="min-width: 50px; max-height: 20px">0%</button>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-3">
                                <div class="row">
                                    <label>P.Pinang</label>
                                </div>
                            </div>
                            <div class="col-sm-2">
                                <div class="row">
                                    <button class="btn btn-sm btn-secondary" type="submit" style="min-width: 50px; max-height: 20px">0%</button>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-3">
                                <div class="row">
                                    <label>Perak</label>
                                </div>
                            </div>
                            <div class="col-sm-2">
                                <div class="row">
                                    <button class="btn btn-sm btn-secondary" type="submit" style="min-width: 50px; max-height: 20px">0%</button>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-3">
                                <div class="row">
                                    <label>Perlis</label>
                                </div>
                            </div>
                            <div class="col-sm-2">
                                <div class="row">
                                    <button class="btn btn-sm btn-secondary" type="submit" style="min-width: 50px; max-height: 20px">0%</button>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-3">
                                <div class="row">
                                    <label>Selangor</label>
                                </div>
                            </div>
                            <div class="col-sm-2">
                                <div class="row">
                                    <button class="btn btn-sm btn-secondary" type="submit" style="min-width: 50px; max-height: 20px">0%</button>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-3">
                                <div class="row">
                                    <label>Terengganu</label>
                                </div>
                            </div>
                            <div class="col-sm-2">
                                <div class="row">
                                    <button class="btn btn-sm btn-secondary" type="submit" style="min-width: 50px; max-height: 20px">0%</button>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-3">
                                <div class="row">
                                    <label>Sabah</label>
                                </div>
                            </div>
                            <div class="col-sm-2">
                                <div class="row">
                                    <button class="btn btn-sm btn-secondary" type="submit" style="min-width: 50px; max-height: 20px">0%</button>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-3">
                                <div class="row">
                                    <label>Sarawak</label>
                                </div>
                            </div>
                            <div class="col-sm-2">
                                <div class="row">
                                    <button class="btn btn-sm btn-secondary" type="submit" style="min-width: 50px; max-height: 20px">0%</button>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-3">
                                <div class="row">
                                    <label>W.P KL</label>
                                </div>
                            </div>
                            <div class="col-sm-2">
                                <div class="row">
                                    <button class="btn btn-sm btn-secondary" type="submit" style="min-width: 50px; max-height: 20px">0%</button>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-3">
                                <div class="row">
                                    <label>W.P Putrajaya</label>
                                </div>
                            </div>
                            <div class="col-sm-2">
                                <div class="row">
                                    <button class="btn btn-sm btn-secondary" type="submit" style="min-width: 50px; max-height: 20px">0%</button>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-3">
                                <div class="row">
                                    <label>W.P Labuan</label>
                                </div>
                            </div>
                            <div class="col-sm-2">
                                <div class="row">
                                    <button class="btn btn-sm btn-secondary" type="submit" style="min-width: 50px; max-height: 20px">0%</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    <!--/.row-->
</div>

</div>
@endsection
<!-- /.conainer-fluid -->

@section('myscript')
@endsection