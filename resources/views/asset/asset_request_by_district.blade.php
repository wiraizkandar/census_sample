@extends('master')
@section('content')

<div class="container-fluid">
    <div class="row">

        <div class="col-sm-4 col-lg-4">
            <div class="col-xs-12 text-center ">
                <input type="text" class="form-control" placeholder="Cari nama dokumen" />
                <br/>
                <h6>Maklumat Baki Aset Setiap Daerah</h6>
            </div>
            <div class="row">
                <div class="col-lg-6">
                    <div class="card bg-light">
                        <div class="text-center card-body pb-0">
                            &nbsp;
                        </div>
                    </div>
                </div>
                <div class="col-lg-6">
                    <div class="card bg-light">
                        <div class="text-center card-body pb-0">
                            &nbsp;
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-6">
                    <div class="card bg-light">
                        <div class="text-center card-body pb-0">
                            &nbsp;
                        </div>
                    </div>
                </div>
                <div class="col-lg-6">
                    <div class="card bg-light">
                        <div class="text-center card-body pb-0">
                            &nbsp;
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-6">
                    <div class="card bg-light">
                        <div class="text-center card-body pb-0">
                            &nbsp;
                        </div>
                    </div>
                </div>
                <div class="col-lg-6">
                    <div class="card bg-light">
                        <div class="text-center card-body pb-0">
                            &nbsp;
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-sm-8 col-lg-8">
            <div class="card">
                <div class="card-header">
                    Permintaan Aset Bagi Daerah</div>
                <div class="card-body">
                    <a href="{{route('asset_request_district.create')}}" class="btn btn-primary">Permintaan</a>
                    <table class="table table-sm table-responsive-lg table-responsive-sm">
                        <thead>
                            <tr>
                                <th>No.</th>
                                <th>Tarikh Permintaan</th>
                                <th>Kod Aset</th>
                                <th>Nama Aset</th>
                                <th>Daerah Pemohon</th>
                                <th>Daerah Dipohon</th>
                                <th>Kuantiti</th>
                                <th>Status</th>
                                <th>Tindakan</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td>1</td>
                                <td>30/09/2018</td>
                                <td>2</td>
                                <td>Soal Selidik</td>
                                <td>Johor Bahru</td>
                                <td>Alor Gajah</td>
                                <td>100</td>
                                <td>
                                    <span class="badge badge-info">Lulus</span>
                                </td>
                                <td>
                                    <i class="icon-pencil" />
                                    <i class="icon-trash" />
                                </td>
                            </tr>
                        </tbody>
                    </table>
                    <ul class="pagination">
                        <li class="page-item">
                            <a class="page-link" href="#">Prev</a>
                        </li>
                        <li class="page-item active">
                            <a class="page-link" href="#">1</a>
                        </li>
                        <li class="page-item">
                            <a class="page-link" href="#">2</a>
                        </li>
                        <li class="page-item">
                            <a class="page-link" href="#">3</a>
                        </li>
                        <li class="page-item">
                            <a class="page-link" href="#">4</a>
                        </li>
                        <li class="page-item">
                            <a class="page-link" href="#">Next</a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
<!-- /.conainer-fluid -->

