<?php

namespace App\Http\Controllers\Document;

use Illuminate\Http\Request;

class DocumentManagementController extends \App\Http\Controllers\Controller
{

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
       
        return view('document.document_management');
    }

    public function create()
    {
        return view('document.document_management_create');
    }

    public function store(\App\Http\Requests\StoreDocument $request)
    {
        //logic
        //model
    }

    /**
     * Edit the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function edit()
    {
        return view('document.document_management_edit');
    }

}
