$(function () {
    'use strict';
    var canvas = document.getElementById("document_statistic_by_state");
    var btn_back = document.getElementById("document_statistic_by_district_btn");
    var ctx1 = document.getElementById("document_statistic_by_state").getContext('2d');
    var myChart1 = new Chart(ctx1, {
        type: 'bar',
        data: {
            labels: ["MALAYSIA", "Johor", "Kedah", "Kelantan", "Melaka", "Negeri Sembilan", "Pahang", "Pulau Pinang"
                        , "Perak", "Perlis", "Selangor", "Terengganu", "Sabah", "Sarawak", "W.P Kuala Lumpur", "W.P Labuan", "W.P Putrajaya"],
            datasets:
                    [{
                            label: 'Selesai',
                            data: [41, 22, 57, 19, 22, 34, 41, 22, 57, 19, 22, 34, 41, 22, 57, 19, 22],
                            backgroundColor: 'blue'
                        },
                        {
                            label: 'Belum Selesai',
                            data: [41, 22, 57, 19, 22, 34, 41, 22, 57, 19, 22, 34, 41, 22, 57, 19, 22],
                            backgroundColor: 'yellow'
                        },
                    ]
        },
        options: {
            legend: {
                display: true,
                position: 'bottom',
            },
            title: {
                display: true,
                text: 'Senarai Penghantaran/Penerimaan Dokumen Mengikut Negeri'
            },
            scales: {
                xAxes: [{
                        ticks: {
                            autoSkip: false,
                            fontSize: 9,
                        },
                        stacked: true,
                    }],
                yAxes: [{
                        ticks: {
                            beginAtZero: true,
                            steps: 10,
                            stepValue: 5,
                            max: 100
                        },
                        stacked: true
                    }]
            },
        }
    });


    var ctx2 = document.getElementById("document_statistic_by_district").getContext('2d');
    var myChart2 = new Chart(ctx2, {
        type: 'horizontalBar',
        data: {
            labels: ["MALAYSIA", "Johor", "Kedah", "Kelantan", "Melaka", "Negeri Sembilan", "Pahang", "Pulau Pinang"
                        , "Perak", "Perlis", "Selangor", "Terengganu", "Sabah", "Sarawak", "W.P Kuala Lumpur", "W.P Labuan", "W.P Putrajaya"],
            datasets:
                    [{
                            label: 'Selesai',
                            data: [41, 22, 57, 19, 22, 34, 41, 22, 57, 19, 22, 34, 41, 22, 57, 19, 22],
                            backgroundColor: 'blue'
                        },
                        {
                            label: 'Belum Selesai',
                            data: [41, 22, 57, 19, 22, 34, 41, 22, 57, 19, 22, 34, 41, 22, 57, 19, 22],
                            backgroundColor: 'yellow'
                        },
                    ]
        },
        options: {
            title: {
                display: true,
                text: 'Senarai Penghantaran/Penerimaan Dokumen Mengikut Negeri'
            },
            scales: {
                xAxes: [{
                        ticks: {
                            autoSkip: false,
                            fontSize: 9,
                        },
                        stacked: true,
                    }],
                yAxes: [{
                        ticks: {
                            beginAtZero: true,
                            steps: 10,
                            stepValue: 5,
                            max: 100
                        },
                        stacked: true
                    }]
            },
        }
    });

    canvas.onclick = function (evt) {
        var activePoints = myChart1.getElementsAtEvent(evt);
        console.log(activePoints)
        if (activePoints[0]) {
            var chartData = activePoints[0]['_chart'].config.data;
            var idx = activePoints[0]['_index'];

            var label = chartData.labels[idx];

            $('#document_statistic_by_state').hide()
            $('#document_statistic_by_district_btn').show()
            $('#document_statistic_by_district').show()

        }
    };


    btn_back.onclick = function (evt) {
        $('#document_statistic_by_state').show()
        $('#document_statistic_by_district_btn').hide()
        $('#document_statistic_by_district').hide()
    }

});
