@extends('master')
@section('content')

<div class="container-fluid">
    <div class="animated fadeIn">
        <div class="row">
            <!-- /.col-->
            <div class="col-lg-12">

                <div class="card">
                    <div class="card-header">
                        <i class="fa fa-align-justify"></i> Maklumat Telefon Pintar</div>
                    <div class="card-body">
                        <div class="row">
                            <div class="col-md-12"  >
                                <div class="form-group row">
                                    <label class="col-md-2 col-form-label" for="select3">Jenis Telefon Pintar(Include: Ipad/tab/phablet)</label>
                                    <div class="col-md-4">
                                        <input class="form-control form-control-sm" id="input1-group2" type="text" name="input1-group2" placeholder="">
                                    </div>
                                    <label class="col-md-2 col-form-label" for="select3">Jenis Data Plan (pra/pasca bayar)</label>
                                    <div class="col-md-4">
                                        <input class="form-control form-control-sm" id="input1-group2" type="text" name="input1-group2" placeholder="">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-md-2 col-form-label" for="select3">OS Telefon (ios/android/windows)</label>
                                    <div class="col-md-4">
                                        <div class="input-group">
                                            <input class="form-control form-control-sm" id="input1-group2" type="date" name="input1-group2" placeholder="Cari Nama Dokumen">
                                            <span class="input-group-prepend">
                                                <button class="btn btn-secondary" type="button">
                                                    <i class="fa fa-calendar"></i></button>
                                            </span>
                                        </div>
                                    </div>
                                </div>
                            </div>  
                        </div>
                    </div>


                </div>
                <div class="card">
                    <div class="card-header">
                        <i class="fa fa-align-justify"></i> Maklumat Waris</div>
                    <div class="card-body">
                        <div class="row">
                            <div class="col-md-12"  >
                                <div class="form-group row">
                                    <label class="col-md-2 col-form-label" for="select3">Nama</label>
                                    <div class="col-md-4">
                                        <input class="form-control form-control-sm" id="input1-group2" type="text" name="input1-group2" placeholder="">
                                    </div>
                                    <label class="col-md-2 col-form-label" for="select3">No Telefon</label>
                                    <div class="col-md-4">
                                        <input class="form-control form-control-sm" id="input1-group2" type="text" name="input1-group2" placeholder="">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-md-2 col-form-label" for="select3">Hubungan</label>
                                    <div class="col-md-4">
                                        <div class="input-group">
                                            <input class="form-control form-control-sm" id="input1-group2" type="date" name="input1-group2" placeholder="Cari Nama Dokumen">
                                            <span class="input-group-prepend">
                                                <button class="btn btn-secondary" type="button">
                                                    <i class="fa fa-calendar"></i></button>
                                            </span>
                                        </div>
                                    </div>
                                    <label class="col-md-2 col-form-label" for="select3">Alamat Waris</label>
                                    <div class="col-md-4">
                                        <textarea class="form-control form-control-sm" ></textarea>
                                    </div>
                                </div>
                            </div>  
                        </div>
                    </div>
                </div>
                <div class="card">
                    <div class="card-header">
                        <i class="fa fa-align-justify"></i> Maklumat Pendidikan</div>
                    <div class="card-body">
                        <div class="row">
                            <div class="col-md-12"  >
                                <div class="form-group row">
                                    <label class="col-md-2 col-form-label" for="select3">Kelayakan Akademik</label>
                                    <div class="col-md-4">
                                        <input class="form-control form-control-sm" id="input1-group2" type="text" name="input1-group2" placeholder="">
                                    </div>
                                    <label class="col-md-2 col-form-label" for="select3">Sijil Tertinggi Pendidikan</label>
                                    <div class="col-md-4">
                                        <input class="form-control form-control-sm" id="input1-group2" type="text" name="input1-group2" placeholder="">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-md-2 col-form-label" for="select3">Bahasa Pertuturan</label>
                                    <div class="col-md-4">
                                        <input class="form-control form-control-sm" id="input1-group2" type="text" name="input1-group2" placeholder="">
                                    </div>
                                    <label class="col-md-2 col-form-label" for="select3">Bahasa Penulisan</label>
                                    <div class="col-md-4">
                                        <textarea class="form-control form-control-sm" ></textarea>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-md-2 col-form-label" for="select3">Bahasa Isyarat</label>
                                    <div class="col-md-4">
                                        <input class="form-control form-control-sm" id="input1-group2" type="text" name="input1-group2" placeholder="">
                                    </div>
                                </div>
                            </div>  
                        </div>
                    </div>

                </div>
                <div class="card">
                    <div class="card-header">
                        <i class="fa fa-align-justify"></i> Maklumat Banci</div>
                    <div class="card-body">
                        <div class="row">
                            <div class="col-md-12"  >
                                <div class="form-group row">
                                    <label class="col-md-2 col-form-label" for="select3">Lokaliti Banci - Negeri</label>
                                    <div class="col-md-4">
                                        <input class="form-control form-control-sm" id="input1-group2" type="text" name="input1-group2" placeholder="">
                                    </div>
                                    <label class="col-md-2 col-form-label" for="select3">Lokaliti Banci - Lingkungan Banci</label>
                                    <div class="col-md-4">
                                        <input class="form-control form-control-sm" id="input1-group2" type="text" name="input1-group2" placeholder="">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-md-2 col-form-label" for="select3">Lokaliti Banci - Daerah Pentadbiran</label>
                                    <div class="col-md-4">
                                        <input class="form-control form-control-sm" id="input1-group2" type="text" name="input1-group2" placeholder="">
                                    </div>
                                    <label class="col-md-2 col-form-label" for="select3">Lokaliti Banci - Blok Penghitungan</label>
                                    <div class="col-md-4">
                                        <input class="form-control form-control-sm" id="input1-group2" type="text" name="input1-group2" placeholder="">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-md-2 col-form-label" for="select3">Lokaliti Banci - Daerah Banci</label>
                                    <div class="col-md-4">
                                        <input class="form-control form-control-sm" id="input1-group2" type="text" name="input1-group2" placeholder="">
                                    </div>
                                    <label class="col-md-2 col-form-label" for="select3">Nama Pegawai Hubungan</label>
                                    <div class="col-md-4">
                                        <input class="form-control form-control-sm" id="input1-group2" type="text" name="input1-group2" placeholder="">

                                    </div>
                                </div>
                            </div>  
                        </div>
                    </div>

                </div>
            </div>
        </div>
        <!-- /.col-->
    </div>
    @endsection
    <!-- /.conainer-fluid -->

